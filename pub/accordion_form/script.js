
function slugify(str,limitWordCount) {
  if(typeof str=='undefined')
    return;
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
  
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes
    if(typeof limitWordCount != 'undefined'){
      const words = str.split('-');
      let slugs = [];
      if(limitWordCount > words.length){
        limitWordCount = words.length;
      }
      for(let i = 0 ; i < limitWordCount;i++){
        slugs.push(words[i]);
      }
      return slugs.join('-')
    }
    return str;
  }
function fixFileUrl(url){
    return url;
}
createAcByParam = (el, _url,displayKey,mJabatan)=>{
    const ndEl = $(el);
    const isJbAc = typeof mJabatan != 'undefined';
    const acParam = ndEl.attr('ac_param') ;
    const url = site_url() + `${_url}?param=${acParam}`;
    // console.log(ndEl,acParam);
    const _displayKey = typeof displayKey != 'undefined' ? displayKey : 'template';
    // set has created ac
    ndEl.attr('init_ac','true');

    //
    var _bloodhoundSuggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // local: suggestions,
        remote: {
        wildcard: '%QUERY',
        url:  `${url}&term=%QUERY`,
        transform: function(argument) {
                console.log('argument', argument)
                return argument
            }
        }
        });

        ndEl.typeahead(null, {
        name: _displayKey,
        display: function(item) {        // display: 'name' will also work
            return item.value;
        },
        limit: 5,
        templates: {
            suggestion: function(item) {
                return  !isJbAc?`<div><div> ${item[_displayKey]}</div></div> `:`<div><div> ${item.nama} - ${item.jabatan}</div></div> `;
            }
        },
        source: _bloodhoundSuggestions.ttAdapter()
        }).on('typeahead:selected', function (e, datum) {
            const template = datum[_displayKey];
            // console.log(template)
            ndEl.val(template);
            ndEl.change();
            ndEl.blur();

            if(isJbAc){
                const nextEl = _displayKey == 'nama' ? 'jabatan' : 'nama';
                $( `input[name=ttd_${nextEl}]`).val(datum[nextEl]);
            }

        });
}
toggleAccordion = (el)=>{
    const p = $(el).parent().next();
    const i = $(el).find('>i.fa');
    if(p.hasClass('in')){
        p.removeClass('in');
        i.removeClass('fa-minus');
        i.addClass('fa-plus');
    }else{
        p.addClass('in');
        
        i.removeClass('fa-plus');
        i.addClass('fa-minus');
    }
};
createInlineTableCell = (el, url)=>{

};
createFormAccordion = (container,title,content)=>{
    let cnt = $(container).find('.modal-body > form');
    let accordionNode = cnt.find('#accordionForm');
    let accordionHtml = `<div class="panel-group" id="accordionForm"></div>`;
    let targetId = MD5(title);
    let accordionItemHtml = `
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="javascript:;"> 
                <span>${title}</span></a>
            </h4>
            <a class="btn-toggle-accordion btn btn-sm btn-default" onclick="toggleAccordion(this)"><i class="fa fa-minus"></i></a>
        </div>
        <div id="${targetId}" class="panel-collapse collapse in">
            <div class="panel-body">
            </div>
        </div>
    </div>
    `;
    if(accordionNode.length == 0){
        accordionNode = $(accordionHtml);
        cnt.prepend(accordionNode);
    }
    let accordionBodyNode = cnt.find(`#${targetId} > .panel-body`);
    if(accordionBodyNode.length == 0){
        accordionNode.append(accordionItemHtml);
        accordionBodyNode = cnt.find(`#${targetId} > .panel-body`);
    }
    accordionBodyNode.append(content);
    
    
};    
function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
toggleCls = (el,delCls,addCls)=>{
    $(el).removeClass(delCls);
    $(el).addClass(addCls);
}
fixField=(item,nd)=>{
    const input = nd.find('input');
    if(input.length>0){
        if(input.attr('type') != 'hidden'){
            input.attr('autocomplete','off')
        }
    }
    if(typeof item == 'object'){
        if(typeof item.suffix != 'undefined'){
            
            const p_input = input.parent();
            const inputGroup = $('<div class="input-group"></div>');
            const inputGroupAddon = $('<span class="input-group-addon"></span>');
            inputGroupAddon.append(item.suffix);
            inputGroup.append(input);
            inputGroup.append(inputGroupAddon);
            p_input.append(inputGroup)
        }
    }
};
gc.onShowForm = (form,state,ajax_url,e) =>{
    // console.log(state)
    if(ajax_url.match(/\/file_upload/)){
        $(form).find('.modal-dialog').removeClass('modal-lg').addClass('modal-sm');
        $(form).find('label:contains(File)').html('&nbsp;');
        $(form).find('.modal-title').text('Unggah / Unduh Custom File');
        return;
    }
    $(form).find('.modal-dialog').removeClass('modal-sm').addClass('modal-lg');
    if(state == 'read'){
        return;
    }
    setTimeout(()=>{
        // Walk through 
        if(typeof gc.table == 'undefined'){
            console.log('no gc.table defined');
            return;
        }
        const formDef = gc.FormDef[gc.table];
        const accordions = formDef.accordions;

        accordions.forEach((row,i)=>{
            const title = row.title;
            const items = row.items;
            let content = '<div>CONTENT</div>';
            
            $.each(items,(j,item)=>{
                let lft,rgt,mid,leftNode,rightNode,midNode,parentNode,parentHtml;
                const isFormField = typeof item == 'string';
                const type = isFormField ? 'form_field' : item.type;
                switch(type){
                    case 'split':
                        lft = item.left;
                        rgt = item.right;
                        parentHtml = `
                            <div class="row">
                                <div class="col-md-6 lft">

                                </div>
                                <div class="col-md-6 rgt">

                                </div>
                            </div>
                        `;
                        parentNode = $(parentHtml);
                        leftNode = parentNode.find('.lft');
                        rightNode = parentNode.find('.rgt');

                        $.each(lft,(k,fieldName)=>{
                            let fName = fieldName;
                            if(typeof fieldName == 'object'){
                                fName = fieldName.field;
                            }
                            let elNd = $(form).find(`#field-${fName}`).closest('.form-group');
                            fixField(fieldName,elNd);
                            leftNode.append(elNd);
                        });
                        $.each(rgt,(k,fieldName)=>{
                            let fName = fieldName;
                            if(typeof fieldName == 'object'){
                                fName = fieldName.field;
                            }
                            let elNd = $(form).find(`#field-${fName}`).closest('.form-group');
                            fixField(fieldName,elNd);
                            rightNode.append(elNd);
                        });
                       
                        createFormAccordion(form, title, parentNode);
                    break;
                    case 'split_3':
                        lft = item.left;
                        mid = item.mid;
                        rgt = item.right;
                        //console.log(lft,rgt);
                        parentHtml = `
                            <div class="row">
                                <div class="col-md-4 lft">

                                </div>
                                <div class="col-md-4 mid">

                                </div>
                                <div class="col-md-4 rgt">

                                </div>
                            </div>
                        `;
                        parentNode = $(parentHtml);
                        leftNode = parentNode.find('.lft');
                        midNode = parentNode.find('.mid');
                        rightNode = parentNode.find('.rgt');

                        $.each(lft,(k,fieldName)=>{
                            let fName = fieldName;
                            if(typeof fieldName == 'object'){
                                fName = fieldName.field;
                            }
                            let elNd = $(form).find(`#field-${fName}`).closest('.form-group');
                            fixField(fieldName,elNd);
                            leftNode.append(elNd);
                        });
                        $.each(mid,(k,fieldName)=>{
                            let fName = fieldName;
                            if(typeof fieldName == 'object'){
                                fName = fieldName.field;
                            }
                            let elNd = $(form).find(`#field-${fName}`).closest('.form-group');
                            fixField(fieldName,elNd);
                            midNode.append(elNd);
                        });
                        $.each(rgt,(k,fieldName)=>{
                            let fName = fieldName;
                            if(typeof fieldName == 'object'){
                                fName = fieldName.field;
                            }
                            let elNd = $(form).find(`#field-${fName}`).closest('.form-group');
                            fixField(fieldName,elNd);
                            rightNode.append(elNd);
                        });
                       
                        createFormAccordion(form, title, parentNode);
                    break;
                    case 'picker':
                    {
                        const id = 'vue_picker_'+MD5(title);
                        const selector = `#${id}`;
                        parentHtml = `
                            <div class="row">
                                <div class="col-md-12 general" id="${id}">
                                    <div class="form-group" v-for="j,i in json">
                                        <label class="col-sm-2 control-label" v-text="i+1+'.'"></label>
                                        <div class="col-sm-8">
                                            <input init_ac="false" ac_param="${btoa(JSON.stringify(item))}" :ac_index="uuidv4()" class="form-control" type="text" v-model="j" @keyup="updateVal(i)" @change="updateVal(i)"  @blur="updateVal(i)"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:;" v-if="(i == (json.length-2) && json.length != 1)|| json.length == 1" class="btn btn-sm btn-success" @click="addRow(json.length-1)"><i class="fa fa-plus"></i></a>
                                            <a href="javascript:;" v-if="i == (json.length-1) && json.length != 1" class="btn btn-sm btn-danger" @click="delRow(i)"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                        `;
                        parentNode = $(parentHtml);

                        rightNode = parentNode.find('.general');
                        let elNd = $(form).find(`#field-${item.field}`).closest('.form-group');
                        rightNode.append(elNd);
                        createFormAccordion(form, title, parentNode);

                        const json_str = elNd.find(`#field-${item.field}`).attr('v-model','json_str').val();
                        elNd.addClass('hide');

                        setTimeout(()=>{
                            window[id] = new Vue({
                            el : selector,
                            data:{
                                json_str:json_str,
                                json:[]
                            },
                            methods:{
                                addRow(){
                                    const index = this.json.push('') - 1;
                                    this.$nextTick(()=>{
                                        this.initAc();
                                    });
                                    return index;
                                },
                                delRow(index){
                                    this.json.splice(index, 1);;
                                },
                                updateVal(i){
                                    const el   = event.target;
                                    const val  = el.value;
                                    this.json[i] = val;
                                    this.json_str = JSON.stringify(this.json);
                                },
                                initAc(){
                                    console.log('initAc')
                                    $(selector).find('input[init_ac=false]').each((index,el)=>{
                                        const param = JSON.parse(atob($(el).attr('ac_param')));
                                        createAcByParam(el, param.url);
                                    });
                                }
                            },
                            mounted(){
                                console.log(`${selector} mounted`);
                                try{
                                    this.json = JSON.parse(this.json_str);
                                }catch{
                                    this.json = [];
                                }
                                this.$nextTick(()=>{
                                    if(this.json.length==0){
                                        this.json = [''];
                                    }
                                    setTimeout(()=>{
                                        this.initAc();
                                    },1000);
                                    
                                });
                            },
                            watch:{
                                json(_newVal,_oldVal){
                                    this.json_str = JSON.stringify(_newVal);
                                }
                            }
                        }) ;
                        },500);
                        
                    }
                        
                    break;

                    case 'inline_table_cell':
                    {
                        const id = 'vue_inline_table_cell_'+MD5(title);
                        const selector = `#${id}`;
                        const columnDef = item.columnDef;
                        const tableHeader = item.tableHeader;
                        const url = site_url()+item.url;
                        const fk_parent = item.fk_parent; 
                        const fk = item.fk; 
                        const pk = item.pk; 
                        let fk_value = -1;
                        try{
                         fk_value = ajax_url.match(/\/edit\/(\d*)/)[1];
                        }catch(e){
                            fk_value = Math.floor(Date.now() / 1000);
                        }

                        parentHtml = `
                            <div class="row">
                                <div class="col-md-12 general" id="${id}">
                                    <table class="table table-bordered table-list">
                                        ${tableHeader.html}
                                        <tbody>
                                        <tr v-for="row,index in json" v-bind:${pk}="json.${pk}">
                                            <td style="text-align:center;width:10px">{{index+1}}.</td>
                                            <td v-bind:style="{width:c.width}" v-if="c.type != 'hidden'" v-for="c,k in colDefParsed">
                                                <input v-if="c.type=='text'||c.type=='number'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"/>
                                                <textarea v-if="c.type=='textarea'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"></textarea>
                                            </td>
                                            <td class="act" style="text-align:center">
                                                <a @click="saveRow(row,index)" v-if="isDirty(index)" class="btn btn-sm btn-success" href="javascript:;"><i class="fa fa-check"></i></a>
                                                <a @click="delRow(row,index)" v-if="!isDirty(index)" class="btn btn-sm btn-danger" href="javascript:;"><i class="fa fa-trash"></i></a>
                                                <a @click="cancelRow(row,index)" v-if="!row.${pk}" class="btn btn-sm btn-warning" href="javascript:;"><i class="fa fa-close"></i></a>
                                            </td>
                                        </tr>
                                        <tr v-if="json.length==0">
                                            <td :colspan="columnCount">Tidak ada data.</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right" :colspan="columnCount">
                                                <a @click="addRow()" class="btn btn-sm btn-info" href="javascript:;"><i class="fa fa-plus"></i> Tambah </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> 
                            </div> 
                        `;
                        parentNode = $(parentHtml);

                        rightNode = parentNode.find('.general');
                        
                        createFormAccordion(form, title, parentNode);

                        

                        setTimeout(()=>{
                            window[id] = new Vue({
                            el : selector,
                            data:{
                                fk_value: fk_value,
                                fk: fk,
                                pk: pk,
                                state: state,
                                fk_parent: fk_parent,
                                columnDef:columnDef,
                                colDefParsed: {},
                                url: url,
                                tableHeader: tableHeader,
                                json:[],
                                newColumn:{},
                                columnCount: 2,
                                dirtyRows:[],
                                tmpJson:{}
                            },
                            methods:{
                                addRow(){
                                    const newColumn = Object.assign({},this.newColumn);
                                    const index = this.json.push(newColumn) - 1;
                                    this.$nextTick(()=>{
                                        $(selector).find('tbody tr:last').prev().find('input:first').focus();
                                    });
                                    this.getTmpJson(index);
                                    return index;
                                },
                                delRow(row,index){
                                    const el = event.target;
                                    const orig_cls = 'fa-trash';
                                    const loading_cls = 'fa-spin fa-spinner';
                                    const i = $(el).find('i.fa').get(0);
                                    swal({
                                      title: "Konfirmasi",
                                      text: 'Hapus baris ?',
                                      type: "warning",
                                      showCancelButton: true,
                                      cancelButtonText:'Batal',
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ya",
                                      closeOnConfirm: true
                                    },
                                    ()=>{
                                       const postData = {};
                                        postData[this.pk] = this.json[index][this.pk];
                                        if($(el).attr('loading') == 'true'){
                                            return;
                                        }
                                        $(el).attr('loading','true');
                                        toggleCls(i,orig_cls,loading_cls);

                                        $.post(`${this.url}/${this.fk_value}?cmd=delete`,postData,(res)=>{
                                            console.log(res);
                                            if(res.success){
                                                this.json.splice(index, 1);
                                                this.unsetTmpJson(index);
                                                this.unsetDirty(index);
                                            }
                                            $(el).attr('loading',false);
                                            toggleCls(i,loading_cls,orig_cls);
                                        },'json'); 
                                    });
                                   
                                },
                                cancelRow(row,index){
                                    this.json.splice(index, 1);
                                    this.unsetTmpJson(index);
                                    this.unsetDirty(index);
                                },
                                saveRow(row,index){
                                    const el = event.target;
                                    const orig_cls = 'fa-check';
                                    const loading_cls = 'fa-spin fa-spinner';
                                    const i = $(el).find('i.fa').get(0);
                                    if($(el).attr('loading') == 'true'){
                                        return;
                                    }
                                    $(el).attr('loading','true');
                                    toggleCls(i,orig_cls,loading_cls);

                                    const newRow    = Object.assign({},this.json[index]);
                                    const postData  = newRow;
                                    postData[this.fk] = this.fk_value;
                                    const oper = typeof postData[this.pk] != 'undefined' ? 'edit':'add';
                                    $.post(`${this.url}/${this.fk_value}?cmd=${oper}`,postData,(res)=>{
                                        console.log(res);
                                        if(res.success){
                                            try{
                                                if(res.data[this.pk]){
                                                    newRow[this.pk] = res.data[this.pk];
                                                }
                                            }catch(e){
                                                console.log(e)
                                            }
                                            
                                            
                                            this.json[index] = newRow;
                                            this.unsetDirty(index);
                                            this.unsetTmpJson(index);
                                            this.getTmpJson(index);
                                        }
                                        $(el).attr('loading',false);
                                        toggleCls(i,loading_cls,orig_cls);
                                    },'json');
                                },
                                isDirty(index){
                                    return $.inArray(index,this.dirtyRows) != -1;
                                },
                                unsetDirty(index){
                                    const dirty_idx = $.inArray(index,this.dirtyRows);
                                    console.log(dirty_idx)
                                    if(dirty_idx != -1){
                                        this.dirtyRows.splice(dirty_idx, 1);
                                    }
                                    console.log(this.dirtyRows)
                                },
                                unsetTmpJson(index){
                                    const tmpJsonKey = MD5(index);
                                    if(typeof this.tmpJson[tmpJsonKey] == 'object'){
                                        this.tmpJson[tmpJsonKey] = null;
                                    }
                                },
                                compareRow(index){
                                    const tmpJson = this.getTmpJson(index);
                                    let isDirty = false;
                                    for( key in this.columnDef){
                                        if(this.json[index][key] != tmpJson[key]){
                                            isDirty = true;
                                            break;
                                        }
                                    };
                                    if(!isDirty){
                                        this.unsetDirty(index);
                                    }else{
                                        if(!this.isDirty(index)){
                                            this.dirtyRows.push(index);
                                        }
                                        
                                    }
                                },
                                onInputClick(index){
                                    const tmpJson = this.getTmpJson(index);
                                    this.compareRow(index);
                                },
                                onInputChanged(row,index){ 
                                    this.compareRow(index);
                                },
                                getTmpJson(index){
                                    const tmpJsonKey = MD5(index.toString());
                                    if(typeof this.tmpJson[tmpJsonKey] != 'object'){
                                        this.tmpJson[tmpJsonKey] = Object.assign({},this.json[index]);
                                    }
                                    return this.tmpJson[tmpJsonKey];
                                },
                                onInputKeyUp(row,index){ 
                                    this.compareRow(index);
                                },
                                getList(){
                                    this.parseColumnDef();
                                    $.get(`${this.url}/${this.fk_value}?cmd=list`,(res)=>{
                                        // console.log(res);
                                        this.json = res.data;
                                    },'json');
                                },
                                parseColumnDef(){
                                    let colDefParsed = {};
                                    let columnCount = 0;
                                    $.each(this.columnDef,(key,val)=>{
                                        columnCount += 1;
                                        // console.log(key,value);
                                        const valueArr = val.split('|');
                                        const value = {
                                            type: valueArr[0],
                                            length: valueArr[1],
                                            label: valueArr[2],
                                            width: typeof valueArr[3] != 'undefined'? `${valueArr[3]}px`:'auto'
                                        }; 
                                        colDefParsed[key] = value;

                                    });
                                    this.colDefParsed = colDefParsed;
                                    this.columnCount = columnCount+2;
                                }
                                
                            },
                            mounted(){
                                this.getList();
                                $(form).find('button[data-dismiss=modal]').click(()=>{
                                    if(this.state=='add'){
                                        $.post(`${this.url}/${this.fk_value}?cmd=delete&temporal=true`,{},(res)=>{
                                                console.log(res);
                                                 
                                        },'json'); 
                                    }
                                    
                                });
                            },
                            watch:{
                                json(_newVal,_oldVal){
                                    // this.json_str = JSON.stringify(_newVal);
                                }
                            }
                        }) ;
                        },500);
                    }
                    break;
                    default:
                    {
                        
                        parentHtml = `
                            <div class="row">
                                <div class="col-md-12 general">

                                </div> 
                            </div> 
                        `;
                        parentNode = $(parentHtml);

                        rightNode = parentNode.find('.general');
                        let fieldName = item;
                        let fName = item;
                        if(typeof fieldName == 'object'){
                            fName = fieldName.field;
                        }
                        let elNd = $(form).find(`#field-${fName}`);
                        if(elNd.length > 0){
                            elNd = elNd.closest('.form-group');
                            fixField(fieldName,elNd)
                            rightNode.append(elNd);
                        }else{
                            try{
                                if($.inArray(item,formDef.upload_fields) != -1){
                                    elNd = $(form).find(`input[name=${item}]`).closest('.form-group');
                                    rightNode.append(elNd);
                                    
                                }
                            }catch(e){
                                console.log(e);
                            }
                        }
                        createFormAccordion(form, title, parentNode);
                        
                    }
                        
                    break;
                }
                
            });
        });
    

        $('input[name=ttd_nama],input[name=ttd_jabatan]').each((i,el)=>{
            $(el).attr('ac_param',$(el).attr('name'))
            createAcByParam(el,`data/ac_jabatan`,$(el).attr('name')=='ttd_jabatan'?'jabatan':'nama',true);
        });
        
        if(typeof gc.__onShowForm == 'function'){
            gc.__onShowForm(form,state,ajax_url,e);
        }
    },10);
}    