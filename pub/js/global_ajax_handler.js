$( document ).ajaxStart(function() {
  $( "#loading" ).html('<i class="fa fa-spin fa-spinner"></i>').show();
});
$( document ).ajaxComplete(function( event, xhr, settings ) {
  // if ( settings.url === "ajax/test.html" ) {
  //   $( ".log" ).text( "Triggered ajaxComplete handler. The result is " +
  //     xhr.responseText );
  // }
});
$( document ).ajaxStop(function() {
  $( "#loading" ).hide();
});
$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
   
    $( ".log" ).html('<i class="fa fa-close"></i>');
 
});
$( document ).ajaxSend(function( event, jqxhr, settings ) {
 
    // $( ".log" ).text( "Triggered ajaxSend handler." );
 
});
$( document ).ajaxSuccess(function( event, xhr, settings ) {
   if(xhr.responseText=='["ERROR_ACCESS_DENIED"]'){
   	alert('Akses ditolak')
   }
   if(xhr.responseText=='["LOGIN_REQUIRED"]'){
   	alert('Anda harus login untuk mengakses halaman ini !');
   	document.location.href = site_url()+'login';
   }
    $( ".log" ).html('<i class="fa fa-check"></i>');
 
});