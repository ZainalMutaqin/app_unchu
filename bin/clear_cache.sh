#!/bin/bash

rm -fv `pwd`/../cache/assets/app_js
rm -fv `pwd`/../cache/assets/top_js
rm -fv `pwd`/../cache/assets/top_css
rm -fv `pwd`/../cache/assets/bottom_js
rm -fv `pwd`/../cache/assets/bottom_css
rm -fv `pwd`/../cache/assets/*.map