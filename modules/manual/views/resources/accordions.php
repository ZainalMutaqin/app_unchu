<div class="panel-group accordion" id="accordion1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> Form Definition</a>
            </h4>
        </div>
        <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <div class="form-group">
                <label>Pilih Tabel</label>
                <div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" v-model="select_table">
                    <option v-for="table in table_list" :value="table" v-text="table"></option>
                </select>
                    </div>
                    <div class="col-md-6">
                         <button :disabled="select_table==''" class="btn btn-danger" @click="dbListFields()"><i class="fa fa-cog"></i> List Field</button>
                    </div>
                </div>
                
               

                <code class="info" style="white-space: pre-wrap;" v-show="message_1.length>0">{{message_1}}</code>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2" aria-expanded="false"> Table Header </a>
            </h4>
        </div>
        <div id="collapse_2" class="panel-collapse collapse" aria-expanded="false">
            <div class="panel-body" style="height:200px; overflow-y:auto;">
                <div class="form-group">
                <label>Pilih Tabel</label>
                <div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" v-model="select_table">
                    <option v-for="table in table_list" :value="table" v-text="table"></option>
                </select>
                    </div>
                    <div class="col-md-6">
                         <button :disabled="select_table==''" class="btn btn-danger" @click="genTableHeaders()"><i class="fa fa-cog"></i> Generate Tabel Header</button>
                    </div>
                </div>
                
              

                <code class="info" style="white-space: pre-wrap;" v-show="message_2.length>0">{{message_2}}</code>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3" aria-expanded="false">Module Generator </a>
            </h4>
        </div>
        <div id="collapse_3" class="panel-collapse collapse" aria-expanded="false">
            <div class="panel-body">
            <!-- START -->
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red" style="display: none;"></i>
                        <span class="caption-subject font-red bold uppercase"> Wizard Pembuatan Module
                            <span class="step-title"> Tahap 1 dari 4 </span>
                        </span>
                    </div>
                    <div class="actions" style="display: none;">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Konfigurasi </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Controllers </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab" class="step active">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Route</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step">
                                            <span class="number"> 4 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Views </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success" style="width: 25%;"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                    <div class="tab-pane active" id="tab1">
                                        <h3 class="block">Variabel - Variabel</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alias
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="alias">
                                                <span class="help-block"> misal: manual-book </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Class
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="klass">
                                                <span class="help-block"> misal: Manual_book</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Path
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="path">
                                                <span class="help-block"> misal: pengaturan atau kosong</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Route
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="route">
                                                <span class="help-block"> misal: manual_book</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Judul Halaman
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="page_title">
                                                <span class="help-block"> misal: Buku Manual</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Subjek
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="subject">
                                                <span class="help-block"> misal: data</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama File Tab
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="tab_filename">
                                                <span class="help-block"> misal: data</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <h3 class="block">Controllers</h3>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alias
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="ctl_alias">
                                                <span class="help-block"> alias </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Class
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="ctl_class">
                                                <span class="help-block"> class </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">CTL
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="ctl_ctl">
                                                <span class="help-block"> ctl </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <h3 class="block">Routes</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">route
                                                <span class="required" aria-required="true"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" v-model="route">
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <h3 class="block">Confirm your account</h3>
                                        <h4 class="form-section">Account</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Username:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="username"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="email"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Profile</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fullname:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="fullname"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Gender:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="gender"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="phone"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="address"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">City/Town:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="city"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Country:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="country"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Remarks:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="remarks"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Billing</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Card Holder Name:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="card_name"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Card Number:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="card_number"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">CVC:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="card_cvc"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Expiration:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="card_expiry_date"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Payment Options:</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="payment[]"> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous disabled" style="display: none;">
                                            <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" @click="next()" class="btn btn-outline green button-next"> Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;" class="btn green button-submit" style="display: none;"> Submit
                                            <i class="fa fa-check"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END -->
               <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" type="text" name="module_name" v-model="module_name" placeholder="Nama Modul" />
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-danger" @click="initModule()"><i class="fa fa-cog"></i> Init Module</button>
                        </div>
                    </div>
                    
               </div>
               <div class="form-group">
               
                <label>Pilih File Konfig</label>
                <div class="row">
                    <div class="col-md-6">
                        <select class="form-control input-sm" v-model="select_table">
                    <option v-for="yml in yml_list" :value="yml" v-text="yml"></option>
                </select>
                    </div>
                    <div class="col-md-6">
                         <button :disabled="select_table==''" class="btn btn-danger" @click="genModules()"><i class="fa fa-cog"></i> Generate Module</button>
                    </div>
                </div>

                <code class="info" style="white-space: pre-wrap;" v-show="message_2.length>0">{{message_3}}</code>
                </div>
            </div>
        </div>
    </div>
     
</div>