<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Pengguna extends Theme_Controller {
	public $_page_title = 'Pengguna';
	public function custom_grid_data()
    {
        $this->load->model('m_pengguna','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $is_active = $field->status == 1;

            $action = "<td class=\"actions\"> 
            <div class='btn-group'>
            <button href=\"".site_url('pengaturan/pengguna/index/edit/'.$field->id_user)."/".slugify($field->name)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button>"."
            <button onclick=\"attachHref('".site_url('pengaturan/pengguna/detail/'.$field->id_user)."/".slugify($field->name)."')\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> 
            </button>
            <button href=\"javascript:;\" disabled class=\"btn btn-sm btn-icon btn-pure ".($is_active?'btn-success':'btn-warning')."\" role=\"button\"> <i class=\"fa ".($is_active?'fa-check':" fa-close")."\" aria-hidden=\"true\"></i> 
            </button>

            </div>
            </td>";
            $no++;
            $row = array();
            $jenis_kelamin = [
                'L' => 'LAKI - LAKI',
                'P' => 'PEREMPUAN'
            ];
            $row[] = $no.'.';
            $row[] = $field->name; 
            $row[] = $field->nama_grup; 
            $row[] = $field->username; 
            $row[] = $field->email; 
            // $row[] = $field->address; 
            // $row[] = $jenis_kelamin[$field->sex]; 
            //thumb($encrypted_path = '', $width = 100, $height = 100, $base64 = ''
            // $avatar_path = 'uploads/avatar/'.$field->image;
            // $encrypted_path = my_simple_crypt($avatar_path, 'e');
            // $avatar_url = site_url() . 'www_static/thumb/'.$encrypted_path.'/100/100';
            // $row[] = sprintf('<img src="%s" alt="%s" class="img-user"/>',$avatar_url, $field->name); 
            
            // $row[] = $field->id_pegawai; 
            $row[] = $field->no_hp;
            $row[] = '<div class="btn-group"><a onclick="displayDetail(event,this)" class="btn btn-info btn-sm" href="'.site_url().'pengaturan/pengguna/penugasan_property/'.$field->id_user.'/'.slugify($field->name).'"><i class="fa fa-home"></i></a></div>'; 
            
             
            // $row[] = $field->status == 0 ? '<i class="fa fa-close"></i>' : '<i class="fa fa-check"></i>';
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/pengguna/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_pengguna.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/pengguna/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_pengguna.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/pengguna.php', $data );
    }
    public function index($oper='')
    {
        $target_yaml = APP . '/form/tb_users.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Pengguna');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_rules('username', 'Username','trim|required|min_length[4]|callback_username_check');
        
        $crud_state = $crud->getState() ;

        if(in_array($crud_state, ['add','insert_validation']) || $this->_check_post_password($crud_state) ) {
            $crud->set_rules('sandi', 'Kata Sandi','trim|required|min_length[6]|callback_sandi_check');
            $crud->set_rules('ulangi_sandi', 'Ulangi Kata Sandi','trim|required|min_length[6]|callback_ulangsandi_check');
        }

        $crud->set_rules('email', 'Email','trim|required|callback_email_check');
        $crud->set_rules('no_hp', 'Nomor HP','trim|required|callback_no_hp_check');

        $crud->set_rules('sex', 'Jenis Kelamin','trim|required');
        $crud->set_rules('address', 'Alamat','trim|required');
        $crud->set_rules('name', 'Nama Lengkap','trim|required');
        $crud->set_rules('level', 'Grup Pengguna','trim|required');
       
        $crud->set_table('tb_users');
		$crud->set_model('m_pengguna');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('no_hp','integer');
        $crud->display_as('name','Nama Lengkap');
        $crud->display_as('username','Username');
        $crud->display_as('address','Alamat');
        $crud->display_as('image','Foto');
        $crud->display_as('level','Grup Pengguna');
        $crud->display_as('id_pegawai','ID Pegawai');
        $crud->display_as('no_hp','Nomor HP');
        $crud->display_as('sex','Jenis Kelamin');
        $crud->display_as('sandi','Kata Sandi');
        $crud->display_as('ulangi_sandi','Ulangi Kata Sandi');
        $crud->fields('name','username','sandi','ulangi_sandi','email','address','sex','no_hp','image','level','status','password');
        $crud->unset_texteditor('address');
        $crud->field_type('sandi','password');
        $crud->field_type('password','hidden');
        $crud->field_type('ulangi_sandi','password');
        $crud->field_type('status','dropdown',[1=>'AKTIF',0=>'TIDAK AKTIF']);
        $crud->field_type('sex','dropdown',['L'=>'LAKI - LAKI','P'=>'PEREMPUAN']);
        $crud->set_relation('level','tb_grup_user','nama_grup');
        
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));

        $upload_dir = 'avatar';
        
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('image', $upload_dir);
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/pengguna.php',$data);
    }
    function _check_post_password($crud_state)
    {
        if(in_array($crud_state, ['edit','update_validation'])){
            $post_password = $_POST['sandi'];
            if(strlen($post_password) > 0){
                return true;
            }
        }
        return false;
    }
    function _set_tgl_entry($post_array){
        $this->load->model('account/m_login');
         

        $dt = date('Y-m-d H:i:s');
        $post_array['password'] = $this->m_login->encrypt_password($post_array['sandi']);
        unset($post_array['sandi']);
        unset($post_array['ulangi_sandi']);

        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $this->load->model('account/m_login');
        

        $dt = date('Y-m-d H:i:s');
        if(isset($post_array['sandi'])){
            $post_array['password'] = $this->m_login->encrypt_password($post_array['sandi']);
            unset($post_array['sandi']);
            unset($post_array['ulangi_sandi']);
        }else{
            unset($post_array['password']);
        }
        

        $post_array['tgl_update'] = $dt;
        // print_r($post_array);
        // exit();
        return $post_array;
    } 
    public function username_check($str)
    {
        $id_user = $this->uri->segment(5);
        if(!empty($id_user) && is_numeric($id_user)){
            $username_old = $this->db->where("id_user",$id_user)->get('tb_users')->row()->username;
            $this->db->where("username !=",$username_old);
        }
        $num_row = $this->db->where('username',$str)->get('tb_users')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('username_check', 'Nama Pengguna tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }
    public function email_check($str)
    {
        $id_user = $this->uri->segment(5);
        if(!empty($id_user) && is_numeric($id_user)){
            $email_old = $this->db->where("id_user",$id_user)->get('tb_users')->row()->email;
            $this->db->where("email !=",$email_old);
        }
        $num_row = $this->db->where('email',$str)->get('tb_users')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('email_check', 'Email sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }
    public function sandi_check($str)
    {
        $this->load->model('account/m_login');

        $valid_password = $this->m_login->valid_password($str);
        
        if (!$valid_password){
            $this->form_validation->set_message('sandi_check', 'Kata Sandi harus berisi kombinasi huruf besar, huruf kecil dan angka.');
            return false;
        }
        else{
            return true;
        }
    }
    public function ulangsandi_check($str)
    {
        
        if ($str != $_POST['sandi']){
            $this->form_validation->set_message('ulangsandi_check', 'Ulangi Kata Sandi harus cocok dengan Kata Sandi');
            return false;
        }
        else{
            return true;
        }
    }
    public function no_hp_check($str)
    {
        $id_user = $this->uri->segment(5);
        if(!empty($id_user) && is_numeric($id_user)){
            $no_hp_old = $this->db->where("id_user",$id_user)->get('tb_users')->row()->no_hp;
            $this->db->where("no_hp !=",$no_hp_old);
        }
        $num_row = $this->db->where('no_hp',$str)->get('tb_users')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('no_hp_check', 'Nomor HP sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }

    public function penugasan_property($id_pengguna)
    {
        $this->load->model('m_pengguna');
        $this->load->model('m_penugasan_property');
        $pengguna = $this->m_pengguna->get_by_id($id_pengguna);
        $penugasan_property_reserved = $this->m_penugasan_property->get_list_reserved();
        $data = [
            'pengguna' => $pengguna,
            'penugasan_property_reserved' => $penugasan_property_reserved,
            'id_pengguna' => $id_pengguna
        ];
        echo $this->load->view('_pengaturan/penugasan_property',$data,true);
        exit();
    }
    public function penugasan_property_save()
    {
        $this->load->model('m_penugasan_property');

       $list = json_decode($this->input->post('list'));
       // print_r($list);
       if(is_array($list)){
        foreach ($list as $row) {
            //
            $check = $this->m_penugasan_property->find($row->id_grup_proyek,$row->id_pengguna);
            $user_id = $this->cms_user_id();

            $this->m_penugasan_property->set_checked($row->id_grup_proyek,$row->id_pengguna,$row->checked,$check,$user_id);
            // sleep(1);
            // echo json_encode($this->m_penugasan_property->find($row->id_grup_proyek,$row->id_pengguna));
            
        }
       }
       echo json_encode(['success'=>true]);
    }

    public function detail($id_pengguna)
    {
        $this->load->model('m_pengguna');
        $pengguna = $this->m_pengguna->get_by_id($id_pengguna);
        $foto = site_url().'themes/metronic/assets/pages/media/profile/profile_user.png';     
        $foto_path = 'uploads/avatar/'.$pengguna['image'];
        if(is_file(BASE.'/'.$foto_path)){
            $foto = site_url() . $foto_path;
        }    

        $data = [
            'pengguna' => $pengguna,
            // 'penugasan_property_reserved' => $penugasan_property_reserved,
            'id_pengguna' => $id_pengguna,
            'foto' => $foto
        ];

        $this->view('_pengguna/detail',$data);
    }
}
