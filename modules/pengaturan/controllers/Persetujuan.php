<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Persetujuan extends Theme_Controller {
	public $_page_title = 'Daftar Persetujuan';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_persetujuan','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('pengaturan/persetujuan/index/edit/'.$field->id_persetujuan)."/".slugify($field->max_persetujuan)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/persetujuan/index/delete/'.$field->id_persetujuan)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            // $row[] = $field->max_persetujuan;
            		// $row[]=$field->id_persetujuan;
        $row[]=$field->nama_pengguna;
		$row[]=$field->grup_pengguna;
		$row[]=format_rupiah($field->max_persetujuan,0);
		$row[]=date('d-m-Y',strtotime($field->tgl_berlaku.' 00:00:01'));
		$row[]=date('d-m-Y',strtotime($field->tgl_berakhir.' 00:00:01'));
		// $row[]=$field->id_user;
		// $row[]=$field->tgl_entry;
		// $row[]=$field->tgl_update;

            $user = $this->m_login->get_by_id($field->id_user);
            $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            $row[] = date('d-m-Y',strtotime($field->tgl_update));
            $row[] = $user->name;
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/persetujuan/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_persetujuan.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/persetujuan/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_persetujuan.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/persetujuan.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_persetujuan.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Persetujuan');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_persetujuan');
		$crud->set_model('m_persetujuan');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->set_relation('id_pengguna','tb_users','name');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
        $crud->display_as('id_pengguna','Nama Pengguna');
        $crud->display_as('max_persetujuan','Batas Maximal Approve');
        $crud->display_as('tgl_berlaku','Tanggal Berlaku');
        $crud->display_as('tgl_berakhir','Tanggal Berakhir');
        $crud->display_as('tgl_berakhir','Tanggal Berakhir');

        $crud->set_rules('id_pengguna','Nama Pengguna','required');
        $crud->set_rules('max_persetujuan','Batas Maximal Approve','required');
        $crud->set_rules('tgl_berlaku','Tanggal Berlaku','required');
        $crud->set_rules('tgl_berakhir','Tanggal Berlaku','required');


    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/persetujuan.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['max_persetujuan'] = preg_replace('/\D+/', '', $post_array['max_persetujuan']);
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        $post_array['max_persetujuan'] = preg_replace('/\D+/', '', $post_array['max_persetujuan']);
        
        return $post_array;
    } 
}
