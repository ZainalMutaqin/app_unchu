<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Rekening_bank extends Theme_Controller {
	public $_page_title = 'Daftar Rekening Bank';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_rekening_bank','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            "."<button href=\"".site_url('pengaturan/rekening_bank/index/edit/'.$field->id_rekening_bank)."/".slugify($field->no_rekening)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/rekening_bank/index/delete/'.$field->id_rekening_bank)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_bank;
            $row[] = $field->no_rekening;
            $row[] = $field->atas_nama;
            $row[] = $field->cabang;
            $row[] = $field->status;
            $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            $row[] = date('d-m-Y',strtotime($field->tgl_update));
            $user = $this->m_login->get_by_id($field->id_user);
            $row[] = $user->name;
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/rekening_bank/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_rekening_bank.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/rekening_bank/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_rekening_bank.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/rekening_bank.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_rekening_bank.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Rekening Bank');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_rekening_bank');
		$crud->set_model('m_rekening_bank');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->set_relation('id_bank','tb_daftar_bank','nama_bank');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
        
        $crud->set_rules('id_bank','Nama Bank','required');
        $crud->set_rules('no_rekening','Nomor Rekening','required');
        $crud->set_rules('atas_nama','Atas Nama','required');
        $crud->set_rules('cabang','Cabang','required');

        $crud->display_as('no_rekening','Nomor Rekening');
        $crud->display_as('atas_nama','Atas Nama');
        $crud->display_as('id_bank','Nama Bank');
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/rekening_bank.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
}
