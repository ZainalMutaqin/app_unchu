<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Hak_akses extends Theme_Controller {
	public $_page_title = 'Hak Akses';
    function _create_checkbox($name,$id_menu,$value){
        return '<input type="checkbox" id_menu="'.$id_menu.'" class="'.$name.'" '.($value?' checked':'').'/>';
    }
	public function custom_grid_data()
    {
        $grup_ids = $this->input->get('grup_ids');
        $grup_id = $this->input->get('grup_id');
        $list = [];
        $this->load->model('m_hak_akses','model');
        if(!empty($grup_ids)  ){
            
            $list = $this->model->get_datatables( $grup_ids);
        }
        
        if(!empty($grup_id)  ){
            
            $list = $this->model->get_datatables($grup_id,true);
        }
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> <div class='btn-group'>
               
            <button href=\"".site_url('pengaturan/hak_akses/index/edit/'.$field->id_hak_akses)."/".slugify($field->nama_1)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> 
             
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/hak_akses/index/delete/'.$field->id_hak_akses)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no.'.';
            $row[] = $field['nama_menu']; 
            $row[] = $this->_create_checkbox('browse',$field['id_menu'],$field['browse']);
            $row[] = $this->_create_checkbox('add',$field['id_menu'],$field['add']); 
            $row[] = $this->_create_checkbox('edit',$field['id_menu'],$field['edit']);
            $row[] = $this->_create_checkbox('delete',$field['id_menu'],$field['delete']); 
            $row[] = $this->_create_checkbox('execute',$field['id_menu'],$field['execute']); 
            // $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/hak_akses/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_hak_akses.php',$tdata);
    }
    private function _customGrid(){
        $this->load->model('m_grup_user');

        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/hak_akses/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_hak_akses.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $data['grup_user'] = $this->m_grup_user->get_dropdown();
        $data['add_to_select'] = $this->input->get('pk');
        
        $this->view('_pengaturan/hak_akses.php', $data );
    }
    public function index()
    {   
        $this->load->model('m_grup_user');
        $target_yaml = APP . '/form/tb_hak_akses.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Hak');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_hak_akses');
		$crud->set_model('m_hak_akses');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->set_relation('id_grup_user','tb_grup_user','nama_grup');
        // $crud->set_relation('id_menu','tb_menu','nama_menu');
        $crud->display_as('id_grup_user','Grup Pengguna');
        $crud->display_as('id_menu','Menu');
       
        $this->load->model('pengaturan/m_menu');
        $crud->field_type('id_menu','dropdown',$this->m_menu->get_dropdown_tree());
        $crud->field_type('browse','dropdown',[1=>'Ya',0=>'Tidak']);
        $crud->field_type('add','dropdown',[1=>'Ya',0=>'Tidak']);
        $crud->field_type('edit','dropdown',[1=>'Ya',0=>'Tidak']);
        $crud->field_type('delete','dropdown',[1=>'Ya',0=>'Tidak']);
        $crud->field_type('execute','dropdown',[1=>'Ya',0=>'Tidak']);

    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->grup_user = $this->m_grup_user->get_dropdown();

		$data->is_admin  = $this->cms_user_group()=='admin';
        $data->add_to_select  = $this->input->get('pk');
		$this->view('_pengaturan/hak_akses.php',$data);
    }
    public function save_credentials()
    {
        $this->load->model('m_hak_akses');

        $grup_ids = $this->input->post('grup_ids');
        $credentials = $this->input->post('credentials');

        // print_r($grup_ids);
        if(is_array($grup_ids)){
            foreach ($grup_ids as $id_grup_user) {
                if(is_array($credentials)){
                    foreach ($credentials as $id_menu => $opsi) {
                        $this->m_hak_akses->check_credential($id_grup_user, $id_menu, $opsi);
                    }
                }
            }
        }
        // print_r($credentials);
        $data = [
            'grup_ids' => $grup_ids,
            'credentials' => $credentials
        ];

        echo json_encode(['success'=>true,'data'=>$data]);
    }
}
