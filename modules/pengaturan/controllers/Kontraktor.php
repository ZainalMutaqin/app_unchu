<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Kontraktor extends Theme_Controller {
	public $_page_title = 'Daftar Kontraktor';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_kontraktor','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('pengaturan/kontraktor/index/edit/'.$field->id_kontraktor)."/".slugify($field->nama_kontraktor)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/kontraktor/index/delete/'.$field->id_kontraktor)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_kontraktor;
            $row[] = $field->alamat;
            $row[] = $field->no_kontrak;
            $row[] = $field->npwp;
            $row[] = $field->status;
            $row[] = !empty($field->legal)?'<a target="_blank" href="'.site_url().'pengaturan/kontraktor/legal/'.$field->id_kontraktor.'/'.slugify($field->nama_kontraktor).'" class="info"><i class="fa  fa-file-pdf-o"</a>':'';

            $row[] = !empty($field->sk_spk)?'<a target="_blank" href="'.site_url().'pengaturan/kontraktor/sk-pk/'.$field->id_kontraktor.'/'.slugify($field->nama_kontraktor).'"class="info"><i class="fa fa-file-text-o"</a>':'';
            // $user = $this->m_login->get_by_id($field->id_kontraktor_user);
            // $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            // $row[] = date('d-m-Y',strtotime($field->tgl_update));
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/kontraktor/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_kontraktor.php',$tdata);
    }
    public function legal($id_kontraktor,$slug)
    {
        $this->load->model('m_kontraktor');
        $kontraktor = $this->m_kontraktor->get_by_id($id_kontraktor);
        if(is_object($kontraktor)){
            redirect(site_url().'uploads/kontraktor/'.$kontraktor->legal);
        }
    }
    public function sk_spk($id_kontraktor,$slug)
    {
        $this->load->model('m_kontraktor');
        $kontraktor = $this->m_kontraktor->get_by_id($id_kontraktor);
        if(is_object($kontraktor)){
            header('Content-Type: text/plain');
            
        }
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/kontraktor/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_kontraktor.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/kontraktor.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_kontraktor.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Kontraktor');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_kontraktor');
		$crud->set_model('m_kontraktor');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->display_as('nama_kontraktor','Nama Kontraktor');
        $crud->display_as('alamat','Alamat Kontraktor');
        $crud->display_as('no_kontrak','Nomor Kontrak');
        $crud->display_as('npwp','NPWP');
        $crud->display_as('legal','Upload Legal');
        $crud->display_as('sk_spk','Syarat & Ketentuan SPK');

        $crud->field_type('sk_spk','text');
        $upload_dir = 'kontraktor';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('legal', $upload_dir);
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/kontraktor.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
}
