<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_grup_proyek  extends Grocery_CRUD_Model  {
	var $table = 'tb_grup_proyek'; 
    private function _get_datatables_query()
    {
        $this->db->select("a.*,
            b.no_rekening,b.nama_kop_surat,b.alamat_kantor b_alamat_kantor,b.logo,b.klausul_sppr,c.no_imb,c.no_izin_lokasi,c.no_pengesahan, c.no_shgb_induk,c.siteplan,p.nama_propinsi propinsi,k.nama_kota kota,kc.nama_kecamatan kecamatan,kl.nama_kelurahan kelurahan")
                 ->join('tb_klausul_sppr b','b.id_klausul_sppr=a.id_klausul_sppr','left')
                 ->join('tb_izin_proyek c','c.id_izin_proyek=a.id_izin_proyek','left')
                 ->join('tb_propinsi p','p.id_propinsi=a.propinsi','left')
                 ->join('tb_kota k','k.id_kota=a.kota','left')
                 ->join('tb_kecamatan kc','kc.id_kecamatan=a.kecamatan','left')
                 ->join('tb_kelurahan kl','kl.id_kelurahan=a.kelurahan','left')
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                { 
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
