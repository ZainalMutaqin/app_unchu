<?php
 
class M_penugasan_property extends CI_Model
{
	public $table= 'tb_penugasan_properti';
	public function get_list_reserved()
	{
		/*
SELECT
tb_grup_proyek.nama_proyek,
Count(tb_penugasan_properti.id_user) AS reserved
FROM
tb_grup_proyek
LEFT JOIN tb_penugasan_properti ON tb_grup_proyek.id_grup_proyek = tb_penugasan_properti.id_grup_proyek
GROUP BY
tb_grup_proyek.nama_proyek
		*/

		$results = $this->db->select('b.id_grup_proyek,a.id_pengguna,b.nama_proyek, COUNT(a.id_pengguna) reserved')
							->from('tb_grup_proyek b')
							->join($this->table.' a','b.id_grup_proyek=a.id_grup_proyek','left')
							->group_by('b.id_grup_proyek')
							->get()
							->result_array();
		return $results;
	}
	public function find($id_grup_proyek, $id_pengguna)
	{
		$results = $this->db->select('b.id_grup_proyek,a.id_pengguna,b.nama_proyek, COUNT(a.id_pengguna) reserved')
							->from('tb_grup_proyek b')
							->where('b.id_grup_proyek',$id_grup_proyek)
							->where('a.id_pengguna',$id_pengguna)
							->join($this->table.' a','b.id_grup_proyek=a.id_grup_proyek','left')
							->group_by('b.id_grup_proyek')
							->get()
							->row();
		return $results;
	}
	public function set_checked($id_grup_proyek,$id_pengguna,$is_checked,$current_row,$user_id){
		// echo($id_grup_proyek).",";
		// echo($id_pengguna).",";
		// echo($is_checked).",";
		// echo($current_row)."\n";
		$dt = date('Y-m-d H:i:s');
		$row = [
			'id_pengguna' => $id_pengguna,
			'id_grup_proyek'=> $id_grup_proyek,
			'tgl_entry' => $dt,
			'tgl_update' => $dt,
			'id_user'=> $user_id
		];
		if($is_checked && empty($current_row)){
			$this->db->insert($this->table,$row);
		}
		if($is_checked && !empty($current_row)){
			$update=[
				'tgl_update'=> $dt
			];
			if($current_row->id_pengguna == $id_pengguna)
				$this->db->update($this->table,$update);
		}
		if(!$is_checked && !empty($current_row)){
			if($current_row->id_pengguna == $id_pengguna)
				$this->db->where('id_pengguna',$id_pengguna)->where('id_grup_proyek',$id_grup_proyek)->delete($this->table);

		}
		
	}
}