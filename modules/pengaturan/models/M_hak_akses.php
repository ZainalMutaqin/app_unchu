<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_hak_akses  extends Grocery_CRUD_Model  {
	var $table = 'tb_hak_akses'; 
    private function _get_datatables_query()
    {
        $this->db->select("a.*,b.nama_menu")
                 ->join('tb_menu b','b.id_menu=a.id_menu','left')
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables($id_grup_user='',$single=false)
    {
         
        $this->load->model('pengaturan/m_menu');

        $menu = $this->m_menu->get_dropdown_tree();
        $result = [];

        foreach ($menu as $id_menu => $nama_menu) {
             $item = [
                'id_menu' => $id_menu,
                'nama_menu' => $nama_menu,
                'browse' => 0,
                'add' => 0,
                'edit' => 0,
                'delete' => 0,
                'execute' => 0,
             ];
             if($single){
                $existing_row = $this->db->select('a.*')
                         ->where('id_grup_user',$id_grup_user)
                         ->where('id_menu',$id_menu) 
                         ->get($this->table.' a')
                         ->row_array();  
                if(is_array($existing_row)){
                    $item['browse'] = $existing_row['browse'];
                    $item['add'] = $existing_row['add'];
                    $item['edit'] = $existing_row['edit'];
                    $item['delete'] = $existing_row['delete'];
                    $item['execute'] = $existing_row['execute'];
                }
             }
             $result[] = $item;
         } 

         return $result;
    }
    function count_filtered()
    {
       
        return $this->count_all();
    }
    public function count_all()
    {
        $this->load->model('pengaturan/m_menu');
        $menu = $this->m_menu->get_dropdown_tree();
        return count($menu);
    }

    public function check_credential($id_grup_user, $id_menu, $opsi){
        $menu = $this->m_menu->get_by_id($id_menu);
        $existing_row = $this->db->select('id_hak_akses')
                                 ->where('id_grup_user',$id_grup_user)
                                 ->where('id_menu',$id_menu)
                                 ->get($this->table)
                                 ->row();
        // die($this->db->last_query());
        $row = [
            'browse' => $opsi['browse'],
            'add' => $opsi['add'],
            'edit' => $opsi['edit'],
            'delete' => $opsi['delete'],
            'execute' => $opsi['execute'],
        ];  
        $dt = date('Y-m-d H:i:s');                       
        if(is_object($existing_row)){
            $id_hak_akses = $existing_row->id_hak_akses;
            $row['tgl_update'] = $dt;

            $this->db->where('id_hak_akses',$id_hak_akses)->update($this->table,$row);
        }else{
            $row['id_grup_user'] = $id_grup_user;
            $row['id_menu'] = $id_menu; 
            $row['tgl_entry'] = $dt;
            $row['tgl_update'] = $dt;
             $this->db->insert($this->table,$row);
        }

    }
}
