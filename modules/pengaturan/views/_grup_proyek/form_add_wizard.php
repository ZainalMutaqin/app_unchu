<?

$field_upload_kvs= [
    'file_imb' => 'uploads/tb_izin_proyek_file_imb',
    'file_pengesahan' => 'uploads/tb_izin_proyek_file_pengesahan',
    'file_shgb_induk' => 'uploads/tb_izin_proyek_file_shgb_induk',
    'siteplan' => 'uploads/tb_izin_proyek_siteplan',
    'file_izin_lokasi' => 'uploads/tb_izin_file_izin_lokasi'
]; 

foreach ($field_upload_kvs as $field_name => $upload_path) {
    $fo = new stdClass();
    $fo->name = $field_name;
    $fo->extras = new stdClass();
    $fo->extras->upload_path = $upload_path;
    $var_name = "upload_input_{$field_name}";
    $$var_name = $crud_izin_proyek->call_protected_method('get_upload_file_input',$fo,'');
}


$views_as_string .=  $crud_izin_proyek->call_protected_method('get_views_as_string');
// echo ($views_as_string);

$field_upload_kvs_2= [
    'logo' => 'uploads/tb_klausul_sppr_logo'
]; 

foreach ($field_upload_kvs_2 as $field_name => $upload_path) {
    $fo = new stdClass();
    $fo->name = $field_name;
    $fo->extras = new stdClass();
    $fo->extras->upload_path = $upload_path;
    $var_name = "upload_input_{$field_name}";
    $$var_name = $crud_klausul_sppr->call_protected_method('get_upload_file_input',$fo,'');
}


$views_as_string .=  $crud_klausul_sppr->call_protected_method('get_views_as_string');
echo ($views_as_string); 
?>
<?php 
foreach($crud_izin_proyek->get_protected_property_value('css_files') as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($crud_izin_proyek->get_protected_property_value('js_files') as $file): ?>
    <?if(!preg_match('/jquery\.fileupload\.config\.js/', $file)):?>
    <script src="<?php echo $file; ?>"></script>
    <?endif?>
<?php endforeach; ?>
<section id="form_add_grup_proyek" style="display: none">
    
    <div class="form-grup-proyek">
        <div class="content-tab">
            <ul class="nav nav-tabs" id="content_tab">
                <li v-for="caption,url in tabs" v-bind:url="url" v-bind:class="{'active':(url==active_item)}">
                    <a v-bind:href="'#'+slugify(caption)" data-toggle="tab" @click="gotoUrl(url)"><i :class="icon[url]"></i> {{caption}} </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_grup_proyek">
                    <h4>Grup Proyek</h4>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Proyek">
                                        <span class="round-tab">
                                            <i class="fa fa-home"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Perizinan dan Siteplan">
                                        <span class="round-tab">
                                            <i class="icon-shield"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Klausul SPPR">
                                        <span class="round-tab">
                                            <i class="icon-book-open"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                        <span class="round-tab">
                                            <i class="icon-check"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="form_items">
                        <div class="modal" tabindex="-1" id="detail_foto_kavling">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0">
                    <div class="row">
                        <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                            <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff"> Form Foto Kavling </span></h4>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div id="lihat-foto_kavling" style="padding: 1em">
                                <form id="foto_kavling_form">
                            
                            <div class="row" v-for="label,index in foto_kavling_rows" :row_index="index" style="padding: 1em">
                                <div class="col-md-6">
                                    <span replace="true" v-text="label" :field_name="label" :row_index="index"></span>
                                                                        
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-danger" :row_index="index" @click="delFotoKavlingRow(index)"><i class="fa fa-minus"></i></a>
                                    
                                </div>
                            </div>
                            <a class="btn btn-primary" @click="addFotoKavlingRow()"><i class="fa fa-plus"></i> Tambah</a>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
                        <form role="form" id="form_grup_proyek" enctype="multipart/form-data" action="{{ site_url }}pengaturan/grup_proyek/izin_proyek/insert">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <div class="form-body">
                                        <h3 class="form-section" style="padding: .5em;padding-left: .6em">Proyek </h3>
                                        <?php
                                        foreach (['nama_proyek','alamat_proyek','propinsi','kota','kecamatan','kelurahan', 'luas_tanah', 'jml_unit', 'klasifikasi', 'konsep', 'segmentasi'] as $field) {
                                        $caption = title_case(str_replace('_',' ',$field));
                                        if($field=='jml_unit'){
                                        $caption = 'Jumlah Unit';
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row"  v-bind:class="{'has-error':verror.<?=$field?>}">
                                                    <label class="control-label col-md-2"><?=$caption?></label>
                                                    <div class="col-md-10">
                                                        <?if(in_array($field, ['propinsi','kota','kecamatan','kelurahan'])):?>
                                                        <select @change="onSelectChange('<?=$field?>')" id="<?=$field?>" v-model="<?=$field?>" name="<?=$field?>"  class="form-control">
                                                            <option :selected="row.value==<?=$field?>" v-for="row in <?='dd_'.$field?>" :value="row.value" v-text="row.label"></option>
                                                        </select>
                                                        <?else:?>
                                                        <?if($field=='luas_tanah'):?>
                                                        <div class="input-group">
                                                            <input id="<?=$field?>" v-model="<?=$field?>" name="<?=$field?>" type="text" class="form-control" placeholder="<?= $caption?>">
                                                            <span class="input-group-addon">
                                                                M<sup>2</sup>
                                                            </span>
                                                        </div>
                                                        <?else:?>
                                                        <input id="<?=$field?>" v-model="<?=$field?>" name="<?=$field?>" type="text" class="form-control" placeholder="<?= $caption?>">
                                                        <?endif?>
                                                        <?endif?>
                                                        <span class="help-block" >{{verror.<?=$field?>}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        <div class="row" style="padding: 2em 0">
                                            <div class="col-md-6">
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6" style="text-align: right;padding-right: 2em">
                                                <button type="button" class="btn btn-primary next-step" @click="validateSelf()">Selanjutnya <i class="fa fa-chevron-right"> </i></button>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <div class="form-body">
                                        <h3 class="form-section" style="padding: .5em;padding-left: .6em">Perizinan dan Siteplan </h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.no_imb}">
                                                    <label class="control-label col-md-3">Nomor IMB</label>
                                                    <div class="col-md-9">
                                                        <input name="no_imb" id="no_imb" type="text" class="form-control" placeholder="Nomor IMB" v-model="no_imb">
                                                        <span class="help-block" v-if="verror.no_imb">{{verror.no_imb}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.file_imb}">
                                                    <label class="control-label col-md-3">File IMB</label>
                                                    <div class="col-md-9">
                                                        <?=$upload_input_file_imb?>
                                                        <span class="help-block" v-if="verror.file_imb">{{verror.file_imb}}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.no_izin_lokasi}">
                                                    <label class="control-label col-md-3">Nomor Izin Lokasi</label>
                                                    <div class="col-md-9">
                                                        <input  name="no_izin_lokasi" type="text" class="form-control" placeholder="Nomor Izin Lokasi" v-model="no_izin_lokasi"/>
                                                        <span class="help-block" v-if="verror.no_izin_lokasi">{{verror.no_izin_lokasi}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.file_izin_lokasi}">
                                                    <label class="control-label col-md-3">File Izin Lokasi</label>
                                                    <div class="col-md-9">
                                                        <?=$upload_input_file_izin_lokasi?>
                                                        <span class="help-block" v-if="verror.file_izin_lokasi">{{verror.file_izin_lokasi}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row"  v-bind:class="{'has-error':verror.no_pengesahan}">
                                                    <label class="control-label col-md-3">Nomor Pengesahan</label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="no_pengesahan" id="no_pengesahan"  class="form-control" placeholder="Nomor Pengsahan" v-model="no_pengesahan">
                                                        <span class="help-block" v-if="verror.no_pengesahan">{{verror.no_pengesahan}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.file_pengesahan}">
                                                    <label class="control-label col-md-3">File Pengesahan</label>
                                                    <div class="col-md-9">
                                                        <?=$upload_input_file_pengesahan?>
                                                        <span class="help-block" v-if="verror.file_pengesahan">{{verror.file_pengesahan}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.no_shgb_induk}">
                                                    <label class="control-label col-md-3">Nomor SHGB Induk</label>
                                                    <div class="col-md-9">
                                                        <input id="no_shgb_induk" name="no_shgb_induk" type="text" class="form-control" placeholder="Nomor SHGB Induk" v-model="no_shgb_induk">
                                                        <span class="help-block"   v-if="verror.no_shgb_induk">{{verror.no_shgb_induk}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row"  v-bind:class="{'has-error':verror.file_shgb_induk}">
                                                    <label class="control-label col-md-3">File SHGB Induk</label>
                                                    <div class="col-md-9">
                                                        <?=$upload_input_file_shgb_induk?>
                                                        <span class="help-block"  v-if="verror.file_shgb_induk">{{verror.file_shgb_induk}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.siteplan}">
                                                    <label class="control-label col-md-3">Gambar Siteplan</label>
                                                    <div class="col-md-9">
                                                        <?=$upload_input_siteplan?>
                                                        <span class="help-block"  v-if="verror.siteplan">{{verror.siteplan}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" v-bind:class="{'has-error':verror.koordinat_map}">
                                                    <label class="control-label col-md-3">Koordinat Google Map</label>
                                                    <div class="col-md-9">
                                                        <input style="margin-bottom: 1em" id="koordinat_map" type="text" class="form-control" placeholder="Pin Lokasi Map" name="koordinat_map" v-model="koordinat_map">
                                                        <div class="google_map_container" style="display:none;width:100%;height:200px;border: solid 1px #ddd">
                                                        </div>
                                                        <span class="help-block"  v-if="verror.koordinat_map">{{verror.koordinat_map}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row" style="padding: 2em 0">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="button" class="btn btn-default prev-step"  @click="prevFirst()"><i class="fa fa-chevron-left"></i> Sebelumnya </button>
                                                            </div>
                                                            <div class="col-md-6">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6" style="text-align: right;padding-right: 2em;">
                                                <button type="button" class="btn btn-primary next-step" @click="validateFirst()">Selanjutnya <i class="fa fa-chevron-right"> </i></button>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="step3">
                                    <h3 style="padding: .5em;padding-left: .6em">Klausul SPPR</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row" v-bind:class="{'has-error':verror.no_rekening}">
                                                <label class="control-label col-md-2">Nomor Rekening</label>
                                                <div class="col-md-10">
                                                    <select @change="onSelectNoRekening()"  name="no_rekening" v-model="no_rekening"  class="form-control">
                                                        <option :selected="row.value==no_rekening" v-for="row in dd_no_rekening" :value="row.value" v-text="row.label"></option>
                                                    </select>
                                                    <span class="help-block"  v-if="verror.no_rekening">{{verror.no_rekening}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row" v-bind:class="{'has-error':verror.nama_kop_surat}">
                                                <label class="control-label col-md-2">Nama Kop Surat</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" placeholder="Nama Kop Surat" name="nama_kop_surat"  v-model="nama_kop_surat" id="nama_kop_surat"/>
                                                    <span class="help-block" v-if="verror.nama_kop_surat">{{verror.nama_kop_surat}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row" v-bind:class="{'has-error':verror.alamat_kantor}">
                                                <label class="control-label col-md-2">Alamat Kantor</label>
                                                <div class="col-md-10">
                                                    <input v-model="alamat_kantor" type="text" class="form-control" placeholder="Alamat Kantor" id="alamat_kantor" name="alamat_kantor">
                                                    <span class="help-block"  v-if="verror.alamat_kantor">{{verror.alamat_kantor}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row" v-bind:class="{'has-error':verror.klausul_sppr}">
                                                <label class="control-label col-md-2">Klausul SPPR</label>
                                                <div class="col-md-10">
                                                    <textarea v-model="klausul_sppr" class="form-control texteditor" placeholder="Klausul SPPR" id="klausul_sppr" name="klausul_sppr"></textarea>
                                                    <span class="help-block"  v-if="verror.klausul_sppr">{{verror.klausul_sppr}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row style">
                                                <label class="control-label col-md-2">Logo & Foto Kavling</label>
                                                <div class="col-md-3" v-bind:class="{'has-error':verror.logo}">
                                                   <?=$upload_input_logo?>
                                                    <span class="help-block" v-if="verror.logo">{{verror.logo}}</span>
                                                </div>
                                                <div class="col-md-7" style="text-align: left" v-bind:class="{'has-error':verror.foto_kavling}">
                                                    <a class="btn btn-primary" @click="toggleFormPhotoKavling()"><i class="fa fa-file-photo-o"></i> Foto kavling</a>
                                                    <span class="help-block" v-if="verror.foto_kavling">{{verror.foto_kavling}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="control-label col-md-2"> </label>
                                                <div class="col-md-10 row" style="padding-top:2em;padding-right: 0">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left" @click="prevSecond()"></i> Sebelumnya </button>
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;padding-right: 0">
                                                        <button type="button" class="btn btn-primary next-step" @click="validateSecond()">Finish <i class="fa fa-chevron-right"> </i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="complete">
                                    <div class="c-container" style="padding: 4em;text-align: center;">
                                        <h3>Selesai</h3>
                                        <p><i class="fa fa-spin fa-spinner"></i> Mohon tunggu sebentar sistem sedang menyimpan data.</p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{ site_url }}modules/pengaturan/assets/grup_proyek/form_add_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{{ site_url }}modules/pengaturan/assets/grup_proyek/form_add_wizard.css">