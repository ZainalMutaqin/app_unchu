 <script src="{{ theme_assets }}global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script> 
 <link rel="stylesheet" type="text/css" href="{{ theme_assets }}global/plugins/select2/css/select2.min.css">
<?$this->view('_pengaturan/tab_pengguna')?>

<h4><i class="fa fa-cog"></i> Hak Akses <?=$group_title?></h4>
<h5>Silahkan Pilih Group Pengguna :</h5>
<div id="select_grup_pengguna" style="padding: 0 0 1em 0">
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
            <div class="col-md-12" style="padding: 0"> 
               <?=form_dropdown('select_grup_pengguna',$grup_user,'','name="grup_users[]" multiple="multiple" class="form-control"')?>

            </div>    
             
           
            
            </div>
        </div>
    
    </div>
</div>
<div class="form-group" style="display: none">
    <input type="checkbox" name="select_all" onclick="setChekAll(this)"> <label class="select_all_label">Pilih Semua</label>
</div>
<iframe src="" id="export_excel" style="display: none"></iframe> 
<div class="modal" tabindex="-1" id="detail_hak_akses">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Isi Hak</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-hak_akses" style="padding: 1em">
                            Memuat Detail 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}
a:hover
{
    text-decoration: underline;
}
th.action{
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
</style>
<?php echo $output; ?>

<div class="form-action" style="text-align: right;padding-top: 1em">
    <div class="btn-group">
        <button style="display: none" class="btn btn-warning" id="btnReload" onclick="reloadCredentials(this)"><i class="fa fa-refresh"></i> Load</button>
        <button style="display: none" class="btn btn-info" id="btnSimpan" onclick="saveCredentials(this)"><i class="fa fa-save"></i> Simpan</button>
       
    </div>
</div>
<script type="text/javascript">
    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    gc.add_to_select = '<?=$add_to_select?>';
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            console.log(res);
            $("#detail_hak_akses").unbind('show.bs.modal');
            $("#detail_hak_akses").on('show.bs.modal', function(){
                $('#lihat-hak_akses').html(res);
            });
            $("#detail_hak_akses").modal("show");
        });
        event.preventDefault();
        return false;
    }
    $('button').click(()=>{
        ref = $('#hak_akses_table').DataTable();
        ref.ajax.reload();
    })
    $(document).ready(function(){
        window.app = new Vue({
            el:'#grid',
            data:{
                filter:{
                },
                is_admin : <?=$is_admin?'true':'false'?>,
                nama_marketing:'',
            },
            mounted(){
                let self = this;
                this.$nextTick(function(){
                    setTimeout(()=>{
                        $('#btnReload,#btnSimpan,#btnClear').unbind('click');
                    },1000);
                    var s2Selector = 'select[name=select_grup_pengguna]';
                    $(s2Selector).select2();
                    $(s2Selector).on('select2:unselect',()=>{
                        updateStateGrup();
                    });
                    $(s2Selector).on('select2:unselecting',()=>{
                        updateStateGrup();
                    });
                    $(s2Selector).on('select2:select', function (e) {
                        
                        updateStateGrup();
                    });
                   
                });

                 if(typeof gc.add_to_select == 'string'){
                    setTimeout(()=>{
                        if(gc.add_to_select.length > 0){
                            var s2Selector = 'select[name=select_grup_pengguna]';
                            $(s2Selector).val([gc.add_to_select]);
                            $(s2Selector).trigger('change');
                            $(s2Selector).trigger('select2:select');
                        }
                    },1000);
                }
            },
            methods:{
                export(){
                    // console.log('export');
                    // let param = JSON.stringify(this.filter);
                    // let url_prxy = site_url()+'pengaturan/hak_akses/export/excel?param='+btoa(param);
                    // $('iframe#export_excel').prop('src',url_prxy);
                },
                reloadData(){
                    var e = $.Event( "keypress", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keydown", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keyup", { which: 13 } );
                    $('input[type=search]').trigger(e);   
                }
            }
        });
    });
gc.table = 'tb_hak_akses';
updateStateGrup = ()=>{
    var grup_ids = getGrupIds();
    if(grup_ids.length){
        $('input[name=select_all]').parent().show();
        loadCheckBox(grup_ids);
        $('#btnSimpan').show();

        if(grup_ids.length == 1){
            $('#btnReload').show();
            reloadCredentials();

        }else{
            $('#btnReload').hide();
        }
    }else{
        $('#btnSimpan').hide();
        $('#btnReload').hide();

        $('input[name=select_all]').parent().hide();

    }
    if(grup_ids.length==0){
        table.ajax.url(site_url()+`pengaturan/hak_akses/custom_grid_data` ).load();
    }
    
};
getGrupIds = ()=>{
    var grup_ids = [];
    var s2Selector = 'select[name=select_grup_pengguna]';
    
    var data = $(s2Selector).select2('data');
    for(var i in data){
        var row = data[i];
        grup_ids.push(row.id);
    }
    return grup_ids;
}
clearCredentials = () =>{

}
saveCredentials = (el) => {
    App.startPageLoading({animate:!0});
    
    
    var icns = $(el).find('i.fa:first').get(0);
    $(el).attr('disabled',true);
    toggleCls(icns,'fa-save','fa-spinner fa-spin');
    var trs = $('#hak_akses_table tbody tr');
    var postData = {credentials:{},grup_ids:[]};
    for(var i = 0; i < trs.length ;i++){
        var b = trs.get(i);
        var inputs = $(b).find('input[type=checkbox]');
        var id_menu = $(inputs.get(0)).attr('id_menu');
        postData.credentials[id_menu] = {

        };
        
        for (var j = 0; j < inputs.length; j++) {
            var k = inputs.get(j);
            var name = $(k).attr('class');
            var ck = $(k).is(':checked') ? 1:0 ;
            postData.credentials[id_menu][name] = ck;
        } 
    }
    postData.grup_ids = getGrupIds();
    // console.log(postData);

    $.post(site_url()+`pengaturan/hak-akses/save_credentials`,postData,(res)=>{
        console.log(res);
        App.stopPageLoading();
        toggleCls(icns,'fa-spinner fa-spin','fa-save');
        $(el).attr('disabled',false);

    },'json');
}
reloadCredentials = () =>{
    var grup_id = getGrupIds()[0];
    console.log(grup_id)
    table.ajax.url(site_url()+`pengaturan/hak_akses/custom_grid_data?grup_id=`+grup_id).load();
}

setChekAll = (el)=>{
    setTimeout(()=>{
        var checked = $(el).is(':checked');
        $('label.select_all_label').text(!checked?'Pilih Semua' : 'Kosongkan Semua');
        $('#hak_akses_table input[type=checkbox]').each((a,b)=>{
            $(b).attr('checked',checked);
        });
    },500);
    
};
loadCheckBox = (grup_ids) =>{
    var grup_id_str = grup_ids.join(',');
    table.ajax.url(site_url()+`pengaturan/hak_akses/custom_grid_data?grup_ids=`+grup_ids);
};

</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
