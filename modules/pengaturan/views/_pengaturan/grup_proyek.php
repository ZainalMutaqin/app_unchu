
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<script type="text/javascript" src="<?=site_url()?>pub/gc/js/jquery_plugins/jquery.numeric.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
<?$this->view('_pengaturan/tab_perumahan')?>
<h4><i class="fa fa-home"></i> Daftar Grup Proyek</h4>

<?$this->load->view('_grup_proyek/form_add_wizard',['crud_klausul_sppr'=>$crud_klausul_sppr,'crud_izin_proyek'=>$crud_izin_proyek])?>

<iframe src="" id="export_excel" style="display: none"></iframe> 

<div class="modal" tabindex="-1" id="detail_grup_proyek">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Isi Grup Proyek</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-grup_proyek" style="padding: 1em">
                            Memuat Detail 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}
a:hover
{
    text-decoration: underline;
}
th.action{
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
</style>
<?php echo $output; ?>
<script type="text/javascript">
    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            console.log(res);
            $("#detail_grup_proyek").unbind('show.bs.modal');
            $("#detail_grup_proyek").on('show.bs.modal', function(){
                $('#lihat-grup_proyek').html(res);
            });
            $("#detail_grup_proyek").modal("show");
        });
        event.preventDefault();
        return false;
    }
    // $('button').click(()=>{
    //     ref = $('#grup_proyek_table').DataTable();
    //     ref.ajax.reload();
    // })
    
gc.table = 'tb_grup_proyek';   
function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
} 
$(document).ready(()=>{
//--------------------
window.loadFile = function(event,next) {
    var target = event.target;
    var file=target.files[0];   
    getBase64(file).then((data)=>{
    var base_64 = data;
    if(target.name.match(/siteplan/)){
        $('.site_plan_container').css({
            background: `url('${base_64}') 0 0 no-repeat`,
            'background-size':'cover'
        })
    }
    if(target.name.match(/logo/)){
        $('.logo_container').css({
            background: `url('${base_64}') 0 0 no-repeat`,
            'background-size':'cover'
        })
    }
    if(target.name.match(/foto_kavling_upload/)){
        $('.foto_kavling_container').css({
            background: `url('${base_64}') 0 0 no-repeat`,
            'background-size':'cover'
        })
    }
    if(typeof next == "boolean"){
        if(next == true){
            var key = $(target).next().attr('name');//.val(base_64);
            window.______FORM[key] = base_64;
        }
    }
   });
    
};


     
});

</script>

