<div class="content-tab">
    <ul class="nav nav-tabs">
        <li url="pengaturan/pengguna">
            <a href="#pengguna" data-toggle="tab">Pengguna </a>
        </li>
        <li url="pengaturan/grup-pengguna">
            <a href="#grup_pengguna" data-toggle="tab">Grup Pengguna</a>
        </li>
        <li url="pengaturan/hak-akses">
            <a href="#hak_akses" data-toggle="tab">Hak Akses</a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(()=>{
        setTimeout(()=>{
            $('.content-tab > ul > li > a').each((a,b)=>{
                $(b).click(function(){
                    document.location.href = site_url()+$(this).parent().attr('url');
                });
            });
            $('.content-tab > ul > li').each((a,b)=>{
                if( $(b).attr('url') == window.breadcrumb.active_item ){
                    $(b).addClass('active');
                }
            });
        },1000);
    });
</script>