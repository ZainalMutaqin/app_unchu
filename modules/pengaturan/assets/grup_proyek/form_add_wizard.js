$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
       /* do_remote_validation();
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
        */

    });
    

    
    $('.content-main-1').append($('section#form_add_grup_proyek'));
function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                return myXhr;
            },
            url: site_url() + 'data/summernote_upload_images',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
            }
        });
}

// update progress bar

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded, max:e.total});
        // reset progress on complete
        if (e.loaded == e.total) {
            $('progress').attr('value','0.0');
        }
    }
} 
function show_upload_button(unique_id, uploader_element)
{
    $('#upload-state-message-'+unique_id).html('');
    $("#loading-"+unique_id).hide();

    $('#upload-button-'+unique_id).slideDown('fast');
    $("input[rel="+uploader_element.attr('name')+"]").val('');
    $('#success_'+unique_id).slideUp('fast');   
}

function load_fancybox(elem)
{
    elem.fancybox({
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   600, 
        'speedOut'      :   200, 
        'overlayShow'   :   false
    });     
}

setTimeout(()=>{
    //-------------------------------------- 
 
      window.______FORM = new Vue({
        el: '#form_items',
        data:{
            map_url:'',
            no_imb : '',
            no_izin_lokasi : '',
            no_pengesahan : '',
            no_shgb_induk : '',
            siteplan : '',
            siteplan_upload : '',
            file_imb_upload : '',
            file_imb : '',
            file_izin_lokasi : '',
            file_izin_lokasi_upload : '',
            file_pengesahan : '',
            file_pengesahan_upload : '',
            file_shgb_induk : '',
            file_shgb_induk_upload : '',
            foto_kavling:'',
            foto_kavling_upload:'',
            dd_no_rekening:'',
            dd_propinsi:[],
            dd_kota:[],
            dd_kecamatan:[],
            dd_kelurahan:[],
            klausul_sppr:'',
            logo_upload:'',
            logo:'',
            nama_kop_surat:'',
            koordinat_map:'',
            no_rekening:'',

            'nama_proyek':'',
            'alamat_kantor':'',
            alamat_proyek:'',
            'propinsi':'',
            'kota':'',
            'kecamatan':'',
            'kelurahan':'', 'luas_tanah':'', 'jml_unit':'','klasifikasi':'', 'konsep':'', 'segmentasi':'',
            verror:{},
            foto_kavling_rows:[]
        },
        mounted(){
            setTimeout(()=>{
                $('#luas_tanah').numeric();
                $('#jml_unit').numeric();
                $(".prev-step").click(function (e) {
                    var active = $('.wizard .nav-tabs li.active');
                    prevTab(active);

                });
                this.submitFormAjax(site_url()+`/pub/json/grup_proyek.json`,{},(res)=>{
                    // console.log(json)
                    // $.each(res,(key,value)=>{
                    //     // console.log(row,b)
                    //     if(!key.match(/upload/))
                    //     this.$data[key] = value;
                    // });

                    setTimeout(()=>{
                        this.loadDD('propinsi','');
                        this.loadDD('no_rekening','');
                        this.loadDD('kabupaten_kota',this.propinsi);
                        this.loadDD('kecamatan',this.kota);
                        this.loadDD('kelurahan',this.kecamatan);

                        this.loadTextEditor();
                        // this.$data.nama_proyek += ' '+uuidv4();
                        this.initFotoKavlingUpload();
                        this.fotoKavlingSessionCheck();
                    },1000);
                },'get');
                this.initFileUpload();
               this.toggleFormPhotokavling
            },1000);
            
           
        },
        // watch:{
        //     // koordinat_map:(a,b)=>{
        //     //     console.log(a,b)
        //     // }
        // },
        methods:{
            toggleFormPhotoKavling(){
                // console.log('fff');
                // let fgp = $('#form_grup_proyek');
                // let ft_kavling = $('#foto_kavling_form')
                // if(fgp.is(':hidden')){
                //     fgp.show();
                // }
                // else if(fgp.is(':visible')){
                //     fgp.hide();
                // }
                // if(ft_kavling.is(':hidden')){
                //     ft_kavling.show();
                // }
                // else if(ft_kavling.is(':visible')){
                //     ft_kavling.hide();
                // }
                // let content = $('#foto_kavling_form').html();
                let content = 'HELLO WORLD';
                $("#detail_foto_kavling").unbind('show.bs.modal');
                $("#detail_foto_kavling").on('show.bs.modal', function(){
                    // $('#lihat-foto_kavling').html(content);
                });
                $("#detail_foto_kavling").modal("show");

            },
            fotoKavlingSessionCheck(parent_id){
                let url = site_url()+'pengaturan/grup-proyek/foto_kavling_upload_session_check';
                if(typeof parent_id != 'undefined'){
                    url += `/${parent_id}`;
                }
                $.get(url,(res)=>{
                    if(res.success){
                        if(res.owners.length>0){
                            for(var i=0;i<res.owners.length;i++){
                                var field_name = res.owners[i];
                                const index = this.foto_kavling_rows.push(field_name) - 1;
                                this.$nextTick(()=>{
                                    this.initGcFileUploader(index);
                                });
                            }
                        }
                    }
                },'json');
            },
            addFotoKavlingRow(){
                var field_name = 'foto_kavling_'+uuidv4().split('-')[0];
                const index = this.foto_kavling_rows.push(field_name) - 1;
                this.$nextTick(()=>{
                    this.initGcFileUploader(index);
                });
                return index;
            },
            delFotoKavlingRow(index){
                console.log(index)
                let field_name = $('div[row_index='+index+'] input.hidden-upload-input').attr('name');
                let filename = $('div[row_index='+index+'] input.hidden-upload-input').val();
                if(filename.length>0){
                    // //////////////////////
                    swal({
                          title: "Konfirmasi",
                          text: "Apakah anda yakin ingin menghapus file "+filename,
                          type: "warning",
                          showCancelButton: true,
                          cancelButtonText:'Batal',
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ya",
                          closeOnConfirm: true
                        },
                        ()=>{
                            let url = site_url()+`pengaturan/grup-proyek/foto_kavling_upload/delete_file/${field_name}/${filename}`;

                            $.ajax({
                                url: url,
                                cache: false,
                                success:(res)=>{
                                    // if(res)
                                    console.log(index)
                                 // this.foto_kavling_rows.splice(index, 1);
                                 // $(`foto_kavling_${index}`).remove(); 
                                 $('div[row_index='+index+'] input.hidden-upload-input').closest(`div[row_index=${index}]`).remove()   
                                }
                            });
                        });
                         
                 

                    // //////////////////////
                }else{
                    this.foto_kavling_rows.splice(index, 1);
                    $(`foto_kavling_${index}`).remove();
                }
                
            },
            initGcFileUploader(index){
                var spans = $('#foto_kavling_form span[replace=true]');
                console.log(spans);
                spans.each((a,_el)=>{
                    console.log(a,_el);
                    var field_name = $(_el).attr('field_name');
                    var url = site_url()+`pengaturan/grup-proyek/foto_kavling_upload_file_input/${field_name}`
                    $.post(url,{},(res)=>{
                        console.log(res);
                        $(_el).replaceWith(res.input);
                        eval(res.js);
                        setTimeout(()=>{
                        this.initFileUpload(true);

                    },100);
                    },'json');
                });
            },
            onSelectChange(key){
                let dd_key = '';
                let parent_id = '';
                if(key == 'propinsi'){
                    dd_key = 'kabupaten_kota';
                    parent_id = this.propinsi;
                }
                if(key == 'kota'){
                    dd_key = 'kecamatan';
                    parent_id = this.kota;

                }
                if(key == 'kecamatan'){
                    dd_key = 'kelurahan';
                    parent_id = this.kecamatan;

                }
                if(key != 'kelurahan')
                this.loadDD(dd_key,parent_id,(res,_key,_parent_id)=>{
                    if(_key=='kabupaten_kota'){
                        _key = 'kota';
                    }
                    var select = $(`select[name=${_key}]`);
                    this.$data[_key]='';
                    select.change();
                });
            },
            loadDD(key,parent_id,callback){
                let dd_key = `dd_${key}`;
                let url = site_url()+`data/${dd_key}`;
                let postData = {parent_id:parent_id};

                $.post(url,postData,(res)=>{
                    if(res.success){
                        if(dd_key == 'dd_kabupaten_kota'){
                            dd_key = 'dd_kota';
                        }
                        this.$data[dd_key] = res.data;
                        
                        if(typeof callback == 'function'){
                            callback(res,key,parent_id);
                        }
                    }
                },'json');
            },
            validateSelf(){
                this.verror={}
                   
                var data = new FormData();
                var fields = ['nama_proyek','alamat_proyek','propinsi','kota','kecamatan','kelurahan', 'luas_tanah', 'jml_unit', 'klasifikasi', 'konsep', 'segmentasi'] ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    data.append(field,this.$data[field]);
                });

                $.ajax({
                    url: site_url() + `pengaturan/grup_proyek/index/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            console.log(res);
                        if(res.success){
                            var $active = $('.wizard .nav-tabs li.active');
                            $active.next().removeClass('disabled');
                            nextTab($active);
                        }else{
                            
                            this.verror = res.error_fields;
                        }
                        
                    },
                    complete:()=>{App.stopPageLoading()}
                });

 

                event.preventDefault();
                return false;
            },
            loadTextEditor(){
                $('.summernote').each(function( index ) {
                  $(this).summernote('destroy');
                });
                $summernote = $('textarea.texteditor').summernote({
                    minHeight: 200,
                        onImageUpload: function(files) {
                            sendFile(files[0])
                          // upload image to server and create imgNode...
                          // $summernote.summernote('insertNode', imgNode);
                      }
                });
                $summernote.on('summernote.image.upload', function(we, files) {
                  // upload image to server and create imgNode...
                  $summernote.summernote('insertNode', imgNode);
                });
            },
            getFormData_upload(field){
                var value = $(`input[name=${field}]`).get(0).files[0];
                if(typeof value =='undefined'){
                    return '';
                }
                return value;
            },
            prevSecond(){

            },
            prevFirst(){

            },
            validateFirst(){
                    this.verror={}
                    var data = new FormData();
                    var fields = ['koordinat_map','no_imb','no_izin_lokasi','no_shgb_induk','no_pengesahan', 'segmentasi','siteplan','file_imb','file_izin_lokasi','file_pengesahan','file_shgb_induk'] ;
                    fields.forEach((field,b)=>{
                        if(typeof this.$data[field] == 'undefined')
                            this.$data[field] = '';
                        data.append(field,this.$data[field]);
                    });
                    
               
                    console.log(data)

                    $.ajax({
                        url: site_url() + `pengaturan/grup_proyek/izin_proyek/insert_validation?is_ajax=true`,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        
                        // contentType: 'multipart/form-data',
                        type: 'POST', // For jQuery < 1.9                        
                        beforeSend:()=>{App.startPageLoading({animate:!0})},
                        success: (res)=>{
                            res = JSON.parse($(res).text());
                            console.log(res);
                            if(res.success){
                                var $active = $('.wizard .nav-tabs li.active');
                                $active.next().removeClass('disabled');
                                nextTab($active);
                            }else{
                                
                                this.verror = res.error_fields;
                            }
                        },
                        complete:()=>{App.stopPageLoading()}
                    });

 

                event.preventDefault();
                return false;
            },
            validateSecond(){
                    this.verror={}
                    var data = new FormData();
                    var fields = ['no_rekening','alamat_kantor','nama_kop_surat','logo','klausul_sppr','foto_kavling'] ;
                    fields.forEach((field,b)=>{
                        if(typeof this.$data[field] == 'undefined')
                            this.$data[field] = '';
                        if(field=='klausul_sppr'){
                            this.$data[field] = $('#klausul_sppr.texteditor').summernote('code');
                        }
                        if(field=='foto_kavling'){
                            this.$data[field] = '-';
                        }
                        data.append(field,this.$data[field]);
                    });
                    
               
                    console.log(data)

                    $.ajax({
                        url: site_url() + `pengaturan/grup_proyek/klausul_sppr/insert_validation?is_ajax=true`,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        
                        // contentType: 'multipart/form-data',
                        type: 'POST', // For jQuery < 1.9                        
                        beforeSend:()=>{App.startPageLoading({animate:!0})},
                        success: (res)=>{
                            res = JSON.parse($(res).text());
                            console.log(res);
                            if(res.success){
                                var $active = $('.wizard .nav-tabs li.active');
                                $active.next().removeClass('disabled');
                                nextTab($active);

                                this.submitForm();
                            }else{
                                
                                
                                this.verror = res.error_fields;
                            }
                        },
                        complete:()=>{App.stopPageLoading()}
                    });

 

                event.preventDefault();
                return false;
            },
            submitForm(){
                var url_self = site_url() + `pengaturan/grup_proyek/index/insert?is_ajax=true`;
                var fields_self = ['nama_proyek','alamat_proyek','propinsi','kota','kecamatan','kelurahan', 'luas_tanah', 'jml_unit', 'klasifikasi', 'konsep', 'segmentasi'] ;
                var url_1st = site_url() + `pengaturan/grup_proyek/izin_proyek/insert?is_ajax=true`;
                var fields_1st = ['koordinat_map','no_imb','no_izin_lokasi','no_shgb_induk','no_pengesahan', 'segmentasi','siteplan','file_imb','file_izin_lokasi','file_pengesahan','file_shgb_induk'];
                var fields_2nd = ['no_rekening','alamat_kantor','nama_kop_surat','logo','foto_kavling','klausul_sppr'] ;
                var url_2nd    = site_url() + `pengaturan/grup_proyek/klausul_sppr/insert?is_ajax=true`;
                var id_izin_proyek = -1;
                var id_klausul_sppr = -1;
                this.submitFormAjax(url_2nd,this.createFormData(fields_2nd),(res)=>{
                    if(res.success){
                        id_klausul_sppr = res.insert_primary_key;
                        this.submitFormAjax(url_1st,this.createFormData(fields_1st),(res)=>{
                            if(res.success){
                                id_izin_proyek = res.insert_primary_key;
                                var data = this.createFormData(fields_self);
                                data.append('id_izin_proyek',id_izin_proyek);
                                data.append('id_klausul_sppr',id_klausul_sppr);
                                this.submitFormAjax(url_self,data,(res)=>{
                                    if(res.success){
                                        console.log(res);
                                        // id_izin_proyek = res.insert_primary_key;
                                    }
                                });
                            }
                        });
                    }
                });
            },
            createFormData(fields){
                var data=new FormData();
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    data.append(field,this.$data[field]);
                });

                return data;
                    
            },
            submitFormAjax(url,data,cbSuccess,type_){
                $.ajax({
                        url: url,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: typeof type_ == 'undefined' ? 'POST':'GET',
                        
                        // contentType: 'multipart/form-data',
                        type: typeof type_ == 'undefined' ? 'POST':'GET', // For jQuery < 1.9                        
                        beforeSend:()=>{App.startPageLoading({animate:!0})},
                        success: (res)=>{
                            if(typeof res=='object'){
                                return cbSuccess(res);
                            }
                            res = JSON.parse($(res).text());
                            cbSuccess(res);
                        },     
                        complete:()=>{App.stopPageLoading()}
                    });

 

            },
            initFotoKavlingUpload(){
                var yourFilesList = [],
                yourParameters = [],
                elem = $("#foto_kavling_form");
                file_upload = elem.fileupload({
                    formData:{extra:1},
                    autoUpload: false,
                    fileInput: $("input:file"),
                }).on("uploadadd", function(e, data) {
                    yourFilesList.push(data.files[0]);
                    yourParameters.push(e.delegatedEvent.target.name);
                });

                $("#foto_kavling_form .send_button").click(function(e) {
                    e.preventDefault();
                    file_upload.fileupload('send', {files:yourFilesList, paramName: yourParameters});
                });
            },
            initFileUpload(foto_kavling){
                /**************************************************************/
                let _self = this;
                let extra_cls = typeof foto_kavling == "boolean" ? '.foto_kavling-upload':'';
                $('.gc-file-upload'+extra_cls).each(function(a,b){
                    var hasFileUpload = $(this).attr('hasFileUpload') == 'yes';
                    if(hasFileUpload){
                        console.log('skipping gc-file-upload '+a)
                        return;
                    }
                    $(this).attr('hasFileUpload','yes'); 
                    var unique_id   = $(this).attr('id');
                    var uploader_url = $(this).attr('rel');
                    var uploader_element = $(this);
                    var x_field_name = uploader_element.next().attr('name');
                    var delete_url  = $('#delete_url_'+unique_id).attr('href');
                    eval("var file_upload_info = upload_info_"+unique_id+"");
                    eval("var string_upload_file = string_upload_file_"+unique_id+"");
                    eval("var string_progress = string_progress_"+unique_id+"");
                    eval("var error_on_uploading = error_on_uploading_"+unique_id+"");
                    eval("var message_prompt_delete_file = message_prompt_delete_file_"+unique_id+"");
                    eval("var error_max_number_of_files = error_max_number_of_files_"+unique_id+"");
                    eval("var error_accept_file_types = error_accept_file_types_"+unique_id+"");
                    eval("var error_max_file_size = error_max_file_size_"+unique_id+"");
                    eval("var error_min_file_size = error_min_file_size_"+unique_id+"");
                    eval("var base_url = base_url_"+unique_id+"");
                    eval("var upload_a_file_string = upload_a_file_string_"+unique_id+"");
                    eval("var string_delete_file = string_delete_file_"+unique_id+"");
                    console.log(unique_id,uploader_url,uploader_element,delete_url,file_upload_info,x_field_name)
                    $(this).fileupload({
                        dataType: 'json',
                        url: uploader_url,
                        cache: false,
                        acceptFileTypes:  file_upload_info.accepted_file_types,
                        beforeSend: function(){
                            let verror = Object.assign({},_self.$data.verror);
                                verror[x_field_name] = false;
                                _self.$data.verror = verror;
                            $('#upload-state-message-'+unique_id).html(string_upload_file);
                            $("#loading-"+unique_id).show();
                            $("#upload-button-"+unique_id).slideUp("fast");
                        },
                        limitMultiFileUploads: 1,
                        maxFileSize: file_upload_info.max_file_size,            
                        send: function (e, data) {                      
                            
                            var errors = '';
                            
                            if (data.files.length > 1) {
                                errors += error_max_number_of_files + "\n" ;
                            }
                            
                            $.each(data.files,function(index, file){
                                if (!(data.acceptFileTypes.test(file.type) || data.acceptFileTypes.test(file.name))) {
                                    errors += error_accept_file_types + "\n";
                                }
                                if (data.maxFileSize && file.size > data.maxFileSize) {
                                    errors +=  error_max_file_size + "\n";
                                }
                                if (typeof file.size === 'number' && file.size < data.minFileSize) {
                                    errors += error_min_file_size + "\n";
                                }                           
                            }); 
                            
                            if(errors != '')
                            {
                                // swal(errors);
                                let verror = Object.assign({},_self.$data.verror);
                                verror[x_field_name] = errors;
                                _self.$data.verror = verror;
                                return false;
                            }
                            
                            return true;
                        },
                        done: function (e, data) {
                            if(typeof data.result.success != 'undefined' && data.result.success)
                            {
                                $("#loading-"+unique_id).hide();
                                $("#progress-"+unique_id).html('');
                                $.each(data.result.files, function (index, file) {
                                    $('#upload-state-message-'+unique_id).html('');
                                    var field_name = uploader_element.attr('name');
                                    $("input[rel="+field_name+"]").val(file.name);
                                    $("input[rel="+field_name+"]").trigger('change');
                                    _self.$data[x_field_name] = file.name;
                                    console.log(file.name);
                                    var file_name = file.name;
                                    
                                    var is_image = (file_name.substr(-4) == '.jpg'  
                                                        || file_name.substr(-4) == '.png' 
                                                        || file_name.substr(-5) == '.jpeg' 
                                                        || file_name.substr(-4) == '.gif' 
                                                        || file_name.substr(-5) == '.tiff')
                                        ? true : false;
                                    if(is_image)
                                    {
                                        $('#file_'+unique_id).addClass('image-thumbnail');
                                        load_fancybox($('#file_'+unique_id));
                                        $('#file_'+unique_id).html('<img src="'+fixFileUrl(file.url)+'" height="50" />');
                                    }
                                    else
                                    {
                                        $('#file_'+unique_id).removeClass('image-thumbnail');
                                        $('#file_'+unique_id).unbind("click");
                                        $('#file_'+unique_id).html(file_name);
                                    }
                                    
                                    $('#file_'+unique_id).attr('href',file.url);
                                    $('#hidden_'+unique_id).val(file_name);

                                    $('#success_'+unique_id).fadeIn('slow');
                                    $('#delete_url_'+unique_id).attr('rel',file_name);
                                    $('#upload-button-'+unique_id).slideUp('fast');
                                });
                            }
                            else if(typeof data.result.message != 'undefined')
                            {
                                swal(data.result.message);
                                show_upload_button(unique_id, uploader_element);
                            }
                            else
                            {
                                swal(error_on_uploading);
                                show_upload_button(unique_id, uploader_element);
                            }
                        },
                        autoUpload: true,
                        error: function()
                        {
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },
                        fail: function(e, data)
                        {
                            // data.errorThrown
                            // data.textStatus;
                            // data.jqXHR;              
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },          
                        progress: function (e, data) {
                            $("#progress-"+unique_id).html(string_progress + parseInt(data.loaded / data.total * 100, 10) + '%');
                        }           
                    });
                    $('#delete_'+unique_id).click(function(){
                        swal({
                          title: "Konfirmasi",
                          text: message_prompt_delete_file,
                          type: "warning",
                          showCancelButton: true,
                          cancelButtonText:'Batal',
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ya",
                          closeOnConfirm: true
                        },
                        function(){
                            var file_name = $('#delete_url_'+unique_id).attr('rel');
                            $.ajax({
                                url: delete_url+"/"+file_name,
                                cache: false,
                                success:function(){
                                    show_upload_button(unique_id, uploader_element);
                                    _self.$data[x_field_name] = '';
                                },
                                beforeSend: function(){
                                    $('#upload-state-message-'+unique_id).html(string_delete_file);
                                    $('#success_'+unique_id).hide();
                                    $("#loading-"+unique_id).show();
                                    $("#upload-button-"+unique_id).slideUp("fast");
                                }
                            });
                        });
                         
                 
                        return false;
                    });         
                    
                });
                /**************************************************************/
            }
        },
        
 
    });
  },3000)

    // //////////////////////////////////////////////////

    //////////////////////////////////////////////////////
});

function nextTab(elem) {
    var e = $(elem).next().find('a[data-toggle="tab"]');
  
        e.click();
   
}
function prevTab(elem) {
    var e = $(elem).prev().find('a[data-toggle="tab"]');//;
     
        e.click();
} 