<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Theme_Controller {
	public $_page_title = 'Dashboard';
	
	public function index()
	{
		$data= [
			'display_name' => $this->cms_display_name()
		];
		$this->view('_dashboard',$data);
	}

	public function set_toggle_session_menu($state)
	{
		$this->session->set_userdata('toggle_session_menu',$state);
	}
}
