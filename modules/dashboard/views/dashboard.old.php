<script type="text/javascript" src="{{ theme_assets }}global/plugins/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ theme_assets }}global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"></script>
<link rel="stylesheet" type="text/css" href="{{ theme_assets }}global/plugins/fullcalendar/fullcalendar.min.css"></script>
<script type="text/javascript" src="{{ theme_assets }}global/plugins/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="{{ theme_assets }}global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"></script>

<div id="dashboard">
<!-- Gambar -->
<div class="modal" tabindex="-1" id="detail_event">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body" style="padding-top: 0">
               	<div class="row">
               		<div class="col-md-12" style="background:#2b3643;padding:.5em  ">
               			<h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Detail Survey</span></h4>
               		</div> 
               	</div>
                <div class="row">
                	<div class="col-md-12">
                		 <div id="lihat-event" style="padding: 1em">
		                	Memuat Detail Event
		                </div>
               
                	</div>
                	 
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- @@@@ -->

<div class="row" style="padding: 1em">
	<div class="form-inline" style="display: none">
		<div class="form-group">
			<input type="text" id="pilih_marketing" class="form-control typeahead" placeholder="Pilih Marketing" v-model="nama_marketing" :disabled="!is_admin"/>
			
		</div>
		<div class="form-group">
			<select id="pilih_cabang" class="form-control" v-model="filter.id_cab">
				<option value="" selected>Pilih Cabang</option>
				<option v-for="(v,k) in dd_cabang" :value="v.id_cab">{{v.cabang}}</option>
				 
			</select>
		</div>
		<div class="form-group input-group-btn">
			<span class="">
			<select id="pilih_cabang" class="form-control" v-model="filter.periode">
				<option value="" selected>Pilih Periode</option>
				<option v-for="year in dd_periode" :value="year">{{year}}</option>
				 
			</select>
                <button class="btn btn-outline green " @click="reloadData()" style="margin-left: 1em">
                    <i class="fa fa-refresh"></i> refresh
                </button>
            </span>
		</div>
	 </div>
</div>
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 blue" href="{{ site_url }}transaksi?status=prospek">
			<div class="visual">
				<i class="fa fa-plus"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" :data-value="statistic.prospek" v-text="statistic.prospek">0</span>
				</div>
				<div class="desc"> Prospek </div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<a class="dashboard-stat dashboard-stat-v2 yellow" href="{{ site_url }}transaksi?status=survey">
			<div class="visual">
				<i class="fa fa-question"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" :data-value="statistic.survey" v-text="statistic.survey">0</span> </div>
					<div class="desc"> Survey </div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-v2 green" href="{{ site_url }}transaksi?status=pelanggan">
				<div class="visual">
					<i class="fa fa-check"></i>
				</div>
				<div class="details">
					<div class="number">
						<span data-counter="counterup" :data-value="statistic.pelanggan" v-text="statistic.pelanggan">0</span>
					</div>
					<div class="desc"> Terpasang  </div>
				</div>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<a class="dashboard-stat dashboard-stat-v2 red" href="{{ site_url }}transaksi?status=batal">
				<div class="visual">
					<i class="fa fa-close"></i>
				</div>
				<div class="details">
					<div class="number">
						<span data-counter="counterup" :data-value="statistic.batal" v-text="statistic.batal">0</span> </div>
						<div class="desc"> Batal Pasang </div>
					</div>
				</a>
			</div>
		</div>
		<div class="row">
			<h4></h4>
		</div>
		<div class="row" style="display: none">
			<div class="col-md-12">
				<div class="calendar">
					<div class="portlet light calendar bordered">
						<div class="portlet-title ">
							<div class="caption">
								<i class="icon-calendar font-dark hide"></i>
								<span class="caption-subject font-dark bold uppercase">Rencana Survey Calon Pelanggan</span>
							</div>
						</div>
						<div class="portlet-body">
						<div id="calendar" class="has-toolbar"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>




</div>

<script type="text/javascript" src="{{ theme_assets }}global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ theme_assets }}global/plugins/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="{{ theme_assets }}global/plugins/fullcalendar/lang-all.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		window.app = new Vue({
			el:'#dashboard',
			data:{
				dd_cabang : <?=json_encode($dd_cabang)?>,
				dd_periode : <?=json_encode($dd_periode)?>,
				filter:{
					id_cab:'',
					id_marketing:'',
					periode:''
				},
				statistic:{
					prospek:0,
					pelanggan:0,
					survey:0,
					batal:0
				},
				dtHasEvents:[],
				dtEventDetail:{},
				is_admin:<?=$is_admin?'true':'false'?>,
				nama_marketing:''
			},
			mounted(){
				this.$nextTick(function(){

					if(!this.is_admin){
						this.nama_marketing = '<?=$default_nama_marketing?>';
						this.filter.id_marketing = '<?=$default_filter_user_id?>';
					}

					// this.getStatistik();
					// this.getKalenderSurvey();
				});
			},
			methods:{
				reloadData(){
					this.getStatistik();
					
				}, 
				getStatistik(){
					App.startPageLoading({animate:!0});

					let url_prxy = site_url() + 'dashboard/statistik';
					var form_data = new FormData();
                    
                    form_data.append('id_marketing',this.filter.id_marketing);
                    form_data.append('id_cab',this.filter.id_cab);
                    form_data.append('periode',this.filter.periode);

 
                    axios({
                        method:'post',
                        url: url_prxy,
                        data:form_data,
                        headers: {'Content-Type': 'multipart/form-data' },

                    })
                    .then((response) => {
                        let data = response.data;
                   
                        this.statistic = data;
                        App.stopPageLoading();
                    })
                    .catch((error) => {
                        try{swal(error.response.data);}catch(e){}
                        App.stopPageLoading();
                        
                    });
				},
				getKalenderSurvey(m,y){
					let url_prxy = site_url() + 'dashboard/kalender_survey';
					let self = this;
					var form_data = new FormData();
                    let dt = new Date();
                    
                    let bulan = dt.getMonth();
                    let tahun = dt.getFullYear();

                    if(typeof m != 'undefined'){
                    	bulan = m;
                    }
                    if(typeof m != 'undefined'){
                    	tahun = y;
                    }
                    form_data.append('bulan',bulan+1);
                    form_data.append('tahun',tahun); 

                    if(!this.is_admin){
                    	form_data.append('id_user',this.filter.id_marketing); 

                    }
					App.startPageLoading({animate:!0});
 					
                    axios({
                        method:'post',
                        url: url_prxy,
                        data:form_data,
                        headers: {'Content-Type': 'multipart/form-data' },

                    })
                    .then((response) => {
                        let data = response.data;
                   		self.updateCalendarEvents(data);
                   		App.stopPageLoading();
                        
                    })
                    .catch((error) => {
                        try{swal(error.response.data);}catch(e){}
                        App.stopPageLoading();
                        
                    });
				},
				updateCalendarEvents(objs){
					// console.log(objs);
					let self = this;
					let __eventsData = [];
					if(objs.length > 0){
						this.dtHasEvents = [];
					}
					for(let i = 0 ; i < objs.length ; i++){
						let obj = objs[i];
						
						let tgl_rencana_survey = this.dateFromMysqlDate(obj.tgl_rencana_survey);
						let tgl_selesai_survey = this.dateFromMysqlDate(obj.tgl_selesai_survey);
						try{
							let tgl_start = obj.tgl_rencana_survey.split(' ')[0];
							
							self.dtHasEvents.push(tgl_start);	
						}catch(e){

						}

						// console.log(`USER_ID:${obj.id_user},PELANGGAN_ID:${obj.id_pelanggan}`);
						let bgcolor = obj.status=='Terlaksana'?"green":"red";
						// console.log(bgcolor)
						let evt = {
							title: `${obj.am_nama_lengkap} : ${obj.nama_pelanggan}`,
	                        start: tgl_rencana_survey,
	                        end: tgl_selesai_survey,
	                        backgroundColor: App.getBrandColor(bgcolor),
	                        __obj__: obj
						};
						// console.log(evt)
						__eventsData.push(evt);
					}
					// console.log(__eventsData);
					if(objs.length > 0){
						$("#calendar").fullCalendar('removeEvents');
						$("#calendar").fullCalendar('addEventSource', __eventsData);
						this.updateDayEvent();
					}
					
				},
				updateDayEvent(){
					$.each(this.dtHasEvents,function(i,tgl_start){
						// console.log(tgl_start);
						/*
						<td class="fc-day-number fc-sun fc-past" data-date="2020-04-05">5</td>
						*/
						let $el = $(`td.fc-day-number[data-date=${tgl_start}]`);
						let txt = $el.text();
							rplc = `<a onclick="gotoDetail('${tgl_start}')" class="btn btn-circle btn-info">${txt}</a>`;
							$el.html(rplc);
					});
					
				},
				dateFromMysqlDate(mysql_string){
					var t, result = null;

				   if( typeof mysql_string === 'string' )
				   {
				      t = mysql_string.split(/[- :]/);

				      //when t[3], t[4] and t[5] are missing they defaults to zero
				      result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);          
				   }

				   return result; 
				}
			}
		});	
	});
	var __eventsData = [];
	var AppCalendar = function() {
    return {
        init: function() {

	var e = new Date
                  , t = e.getDate()
                  , a = e.getMonth()
                  , n = e.getFullYear()
                  , r = {};
	__eventsData = [
					// {
     //                    title: "Acara Sepanjang Hari",
     //                    start: new Date(n,a,1),
     //                    backgroundColor: App.getBrandColor("yellow")
     //                }, {
     //                    title: "Acara Panjang",
     //                    start: new Date(n,a,t - 5),
     //                    end: new Date(n,a,t - 2),
     //                    backgroundColor: App.getBrandColor("green")
     //                }, {
     //                    title: "Acara Berulang",
     //                    start: new Date(n,a,t - 3,16,0),
     //                    allDay: !1,
     //                    backgroundColor: App.getBrandColor("red")
     //                }, {
     //                    title: "Acara Berulang",
     //                    start: new Date(n,a,t + 4,16,0),
     //                    allDay: !1,
     //                    backgroundColor: App.getBrandColor("green")
     //                }, {
     //                    title: "Pertemuan",
     //                    start: new Date(n,a,t,10,30),
     //                    allDay: !1
     //                }, {
     //                    title: "Makan Siang",
     //                    start: new Date(n,a,t,12,0),
     //                    end: new Date(n,a,t,14,0),
     //                    backgroundColor: App.getBrandColor("grey"),
     //                    allDay: !1
     //                }, {
     //                    title: "Pesta Ulang Tahun",
     //                    start: new Date(n,a,t + 1,19,0),
     //                    end: new Date(n,a,t + 1,22,30),
     //                    backgroundColor: App.getBrandColor("purple"),
     //                    allDay: !1
     //                }, {
     //                    title: "Click untuk Google",
     //                    start: new Date(n,a,28),
     //                    end: new Date(n,a,29),
     //                    backgroundColor: App.getBrandColor("yellow"),
     //                    url: "http://google.com/"
     //                }
            ];
            this.initCalendar();

            setTimeout(function(){
            	$('.fc-prev-button span').click(function(){
				   let month = $("#calendar").fullCalendar('getDate').month();
				   let year  = $("#calendar").fullCalendar('getDate').year();
				   app.getKalenderSurvey(month-1,year);
				});

				$('.fc-next-button span').click(function(){
				   let month = $("#calendar").fullCalendar('getDate').month();
				   let year  = $("#calendar").fullCalendar('getDate').year();
				   app.getKalenderSurvey(month+1,year);
				});

            },1000);
        },
        initCalendar: function() {
            if (jQuery().fullCalendar) {
                
                App.isRTL() ? $("#calendar").parents(".portlet").width() <= 720 ? ($("#calendar").addClass("mobile"),
                r = {
                    right: "title, prev, next",
                    center: "",
                    left: "agendaDay, agendaWeek, month, today"
                }) : ($("#calendar").removeClass("mobile"),
                r = {
                    right: "title",
                    center: "",
                    left: "agendaDay, agendaWeek, month, today, prev,next"
                }) : $("#calendar").parents(".portlet").width() <= 720 ? ($("#calendar").addClass("mobile"),
                r = {
                    left: "title, prev, next",
                    center: "",
                    right: "today,month,agendaWeek,agendaDay"
                }) : ($("#calendar").removeClass("mobile"),
                r = {
                    left: "title",
                    center: "",
                    right: "prev,next,today,month,agendaWeek,agendaDay"
                });
               
              
                $("#calendar").fullCalendar("destroy"),
                $("#calendar").fullCalendar({
                    header: r,
                    defaultView: "month",
                    slotMinutes: 15,
                    editable: 0,
                    droppable: 0,
                    lang:'id',
                   
                    events: __eventsData,
                    eventClick: displayEventDetail,
                    viewRender: (function () {
					    var lastViewName;
					    return function (view) {
					        var view = $('#calendar').fullCalendar('getView');
					        // console.log(view.name);
					        if(view.name == 'month'){
					        	app.updateDayEvent();
					        }
					    }
					})(), 
                })
            }
        }
    }
}();
const gotoDetail = (tgl) => {
	document.location.href = site_url() + 'rencana_survey?date='+ tgl.toIdDate();
};
const displayEventDetail = (data, event, view) => {
	console.log(data,event,view);
    var content = '<h3>'+data.title+'</h3>' + 
        '<p><b>Start:</b> '+data.start+'<br />' + 
        (data.end && '<p><b>End:</b> '+data.end+'</p>' || '');
    let row = data.__obj__;    
    var content = `
<h4>Marketing</h4>    
<table class="table table-stripped">
	<tr>
		<td style="width:128px">Nama</td>
		<td>: <span class="txt-nama_marketing">${row.am_nama_lengkap}</span></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>: ${row.am_email}</td>
	</tr>
	<tr>
		<td>Role</td>
		<td>: <span class="txt-role">${row.am_role}</span></td>
	</tr>
</table>
<h4>Pelanggan</h4>    
<table class="table table-stripped">
	<tr>
		<td style="width:128px">Nama</td>
		<td>: <span class="txt-nama_pelanggan">${row.nama_pelanggan}</span></td>
	</tr>
		<td style="width:128px">Kode</td>
		<td>: ${row.kode_pelanggan}</td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>: ${row.alamat} No. Rumah ${row.no_rumah}  RT ${row.rt} RW ${row.rw}<br/>&nbsp;&nbsp;Kel. ${row.kelurahan} Kec. ${row.kecamatan} Kab. ${row.kabupaten}</td>
	</tr>
	<tr>
		<td>Daya Listrik</td>
		<td>: ${row.daya} VA</td>
	</tr>
	<tr>
		<td>Cabang</td>
		<td>: ${row.cabang}</td>
	</tr>
	<tr>
		<td>Status Pelanggan</td>
		<td>: <span class="stt-${row.status_pelanggan}">${row.status_pelanggan.toUpperCase()}</span></td>
	</tr>
	<tr>
		<td>Tgl Rencana</td>
		<td>: ${row.tgl_rencana_survey.toIdDateTime()} <i>s.d</i>  ${row.tgl_selesai_survey.toIdDateTime()} </td>
	</tr>
	<tr>
		<td>Status Survey</td>
		<td>: <span class="stt-${row.status.toLowerCase()}">${row.status.toUpperCase()}</span>  </td>
	</tr>
</table>    
    `;    
    // swal(content)
    $("#detail_event").unbind('show.bs.modal');
    $("#detail_event").on('show.bs.modal', function(){
    	$('#lihat-event').html(content);
    });
    $("#detail_event").modal("show");

}; 
String.prototype.toIdDateTime = function() {
	try{
		let dt = this.split(' ');
		let tm = dt[1];
		    dt = dt[0];
		    tm = tm.split(':');
		    tm = tm[0]+'.'+tm[1];
		dt = dt.split('-');
		// if(dt.length==3){
		dt = dt[2]+'-'+dt[1]+'-'+dt[0];
		// }

		return `${dt} ${tm}`;
	}catch(e){
		return this;
	}
};
String.prototype.toIdDate = function() {
	try{
        let dt = this.split('-');
        if(dt.length==3){
            return dt[2]+'-'+dt[1]+'-'+dt[0];
        }
    }catch(e){
        return '0000-00-00';
    }
};
jQuery(document).ready(function() {
    /*AppCalendar.init();

    var bloodhoundSuggestions = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  // local: suggestions,
	  remote: {
	  	wildcard: '%QUERY',
	    url: site_url()+'data/ac_marketing?term=%QUERY',
	    transform: function(argument) {
	            // console.log(argument)
	            return argument
	        }
	  }
	});

	$('.typeahead').typeahead(null, {
		name:'Marketing',
	    display: function(item) {        // display: 'name' will also work
	        return item.value;
	    },
	    limit: 5,
	    templates: {
	        suggestion: function(item) {
	            return '<div>'+ item.value +'</div>';
	        }
	    },
	    source: bloodhoundSuggestions.ttAdapter()
	}).on('typeahead:selected', function (e, datum) {
	    app.filter.id_marketing = datum.id;
	    app.nama_marketing = datum.value;
	});
	$('.typeahead').keyup(function(){
		if(this.value.length == 0){
			app.filter.id_marketing = '';
	    	app.nama_marketing = '';
		}
	});
	*/

});

</script>
