<div id="dashboard">
<!-- Gambar -->
<div class="modal" tabindex="-1" id="detail_event">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body" style="padding-top: 0">
               	<div class="row">
               		<div class="col-md-12" style="background:#2b3643;padding:.5em  ">
               			<h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Detail Survey</span></h4>
               		</div> 
               	</div>
                <div class="row">
                	<div class="col-md-12">
                		 <div id="lihat-event" style="padding: 1em">
		                	Memuat Detail Event
		                </div>
               
                	</div>
                	 
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- @@@@ -->
<div class="row">
  <div class="col-md-12">
    <h4>Selamat Datang {{ display_name }},  </h4>

   </div>
</div>
<div class="row">
  <div class="col-md-12">
    <form action="#" class="form-horizontal form-bordered">
      <div class="form-group">
        <div class="row">
          <div class="col-md-6" style="">
            <div class="row">
              <label class="control-label col-md-4" style="width:125px;text-align: left;padding-left: 2em">Pilih Tanggal</label>
              <div class="col-md-8" >
                  <div class="input-group  date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                              <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                      <input type="text" class="form-control" name="from">
                      <span class="input-group-addon" style="border:none;background: none"> s.d </span>
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                              <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                      <input type="text" class="form-control" name="to"> </div>
              </div>
            </div>
          </div>
          <div class="col-md-6" style="">
             <div class="row">
            <label class="control-label col-md-2" style="text-align: left;width: 115px">Pilih Proyek</label>
            <div class="col-md-5">
                <select class="form-control">
                  <option id_grup_proyek="1">Al Akutsar Moslem City</option>
                </select>
            </div>  
            <div class="col-md-3">
              <button class="btn btn-circle btn-success"><i class="fa fa-file-excel-o"></i> Expor Excel</button>
            </div>
            </div> 
          </div>
        </div>
      </div>        
    </form>
  </div>
</div>