<?php
 
class M_login extends CI_Model
{
	
 

	public function process_login($username,$password){
		$passwd = $this->encrypt_password($password);
         
 		$login = $this->db->select("a.*,b.role")
           ->join('tb_grup_user b','b.id_grup_user=a.level')
           ->group_start()
           ->where('a.username',$username)
           ->or_where('a.email',$username)
           ->group_end()
           ->group_start()
           ->where('a.password', $passwd)
           ->group_end()
           ->where('a.status',1)
           ->get('tb_users a')
           ->row();	

		return $login;

	}    

  public function get_by_id($id_user,$is_active=1)
  {
    $login = $this->db->select("a.*, a.id_user id_pengguna,b.role,b.nama_grup grup_user")
           ->join('tb_grup_user b','b.id_grup_user=a.level')
          
           ->where('a.id_user', $id_user)
          
           ->where('a.status',$is_active)
           ->get('tb_users a')
           ->row(); 
    return $login;
  }

  public function valid_password($str)
  {
    $uppercase = preg_match('@[A-Z]@', $str);
    $lowercase = preg_match('@[a-z]@', $str);
    $number    = preg_match('@[0-9]@', $str);

    return $uppercase && $lowercase && $number;
  }
  public function encrypt_password($str)
  {
    return md5($this->config->item('encryption_key').$str.'manis-legi'.$str);
  }
}