<?php

class Register extends Theme_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->session->unset_userdata('account');
		redirect('login');
	}
	public function register($value='')
    {
    	if($this->input->get('unique_validation') == 'true'){
    		$nik 	  = $this->input->post('nik');
    		$email 	  = $this->input->post('email');
    		$nomor_hp = $this->input->post('nomor_hp');

    		$pass_email = $this->_unique_email($email);
    		$pass_nik = $this->db->where('nik',$nik)->get('account_register')->num_rows() == 0;
    		$pass_nomor_hp = $this->_unique_nomor_hp($nomor_hp);

    		$response = [
    			'success' => $pass_email   && $pass_nik  && $pass_nomor_hp ,
    			'message' => (!$pass_email    ? 'Email Sudah Digunakan' . "\n":'').
    						 (!$pass_nik   ? 'NIK Sudah Digunakan' . "\n":'').
    						 (!$pass_nomor_hp  ? 'Nomor HP Sudah Digunakan' . "\n":'')
    		];
    		die(json_encode($response));
    	}
    	if($this->input->get('resend_email') == 'true'){
    		$owner = $this->input->get('owner');
    		$data_64 = $this->input->get('data');
    		$json = base64_decode($data_64);
    		$obj = json_decode($json);
    		if(is_object($obj)){
    			$data = (array)$obj;
    			$this->_register_send_email($owner, $data);
    			echo "Resend Email Berhasil";
    		}
    		
    		die();
    	}
    	 
		if($this->input->post()){ //detect input post only
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required');
			$this->form_validation->set_rules('email','Alamat Email','required');
			$this->form_validation->set_rules('nik','NIK','required');
			$this->form_validation->set_rules('nomor_hp','Nomor HP','required');
			$this->form_validation->set_rules('password','Kata Sandi','required');
			$this->form_validation->set_rules('repeat_password','Ulangi Kata Sandi','required');
            
            if(APP_YML['app_enable_google_captcha']){
				$this->form_validation->set_rules('g-recaptcha-response','Rechaptcha','required');
            }


			if($this->form_validation->run() != false){

            	if(APP_YML['app_enable_google_captcha']){

					if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
					{
					    $secret = APP_YML['app_google_captcha_secret'];
					    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					    $responseData = json_decode($verifyResponse);
					    if($responseData->success)
					    {
					        $succMsg = 'Your contact request have submitted successfully.';

					    }
					    else
					    {
					        $errMsg = 'Robot verification failed, please try again.';
					        die($errMsg);
					    }
					}else{
						$errMsg = 'Robot verification failed, please try again.';
					        die($errMsg);
					}
				}	

				$nama_lengkap = $this->input->post('nama_lengkap');
				$password = $this->input->post('password');
				$email = $this->input->post('email');
				$nik = $this->input->post('nik');
				$repeat_password = $this->input->post('repeat_password');
				$nomor_hp = $this->input->post('nomor_hp');

				$new_account = [
					'nama_lengkap' => $nama_lengkap,
					'email' => $email,
					'nik' => $nik,
					'password' => $password,
					'nomor_hp'=>$nomor_hp,
					'username'=>slugify($nama_lengkap).md5(microtime())
				];

				if($register = $this->_proces_register($new_account,$msg)){
					return $this->_register_success($register);
				}else{
					return $this->_register_failed($msg);
				}
			}else{
				return $this->_register_validation_error();
			}
	 
		}
	 
		$this->__site_layout = 'register_layout';
		$this->view('register_page',['register_failed'=>false,'validation_error'=>'']);
    }
    
	function _proces_register( $new_account, &$msg)
    {
		$owner = md5(microtime());
    	 

		$config['upload_path'] = BASE.'/uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
    
		$this->load->library('upload', $config);
    
    	$field_name = "nik_file";
        
		if ( ! $this->upload->do_upload($field_name))
		{
			$msg =  $this->upload->display_errors() ;
			return false;
		}
    	$field_name2 = "ktp_selfi_file";

		$uploaded_file = $this->upload->data();

		if ( ! $this->upload->do_upload($field_name2))
		{
			$msg =  $this->upload->display_errors() ;
			@unlink(BASE . '/uploads/'.$uploaded_file['file_name']);
			return false;
		}
		$uploaded_file2 = $this->upload->data();
		

		$upload = [
			'rules' => 'register',
			'desc'=>'KTP',
			'owner' => $owner,
			'file_name' => $uploaded_file['file_name'],
			'file_type' => $uploaded_file['file_type'],
			'file_ext' => $uploaded_file['file_ext'],
			'is_image' => 1,
			'file_size' => $uploaded_file['file_size'],
			'image_width'=>$uploaded_file['image_width'],
			'image_height'=>$uploaded_file['image_height'],
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/'.$uploaded_file['file_name']
		];
		$this->db->insert('uploads',$upload);
		$ktp_id = $this->db->insert_id();

		$upload2 = [
			'rules' => 'register',
			'desc'=>'KTP Selfi',
			'owner' => $owner,
			'file_name' => $uploaded_file2['file_name'],
			'file_type' => $uploaded_file2['file_type'],
			'file_ext' => $uploaded_file2['file_ext'],
			'is_image' => 1,
			'file_size' => $uploaded_file2['file_size'],
			'image_width'=>$uploaded_file2['image_width'],
			'image_height'=>$uploaded_file2['image_height'],
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/'.$uploaded_file2['file_name']
		];
		$this->db->insert('uploads',$upload2);
		$ktp_selfi_id = $this->db->insert_id();

		$files = [
			'ktp'=> $ktp_id,
			'ktp_selfi' => $ktp_selfi_id
		];

		// $this->db->where('email',$new_account['email'])->where('is_active',0)->delete('account_register');

		$account_register = [
			'nik' => $new_account['nik'],
			'passwd' => $this->_encrypt_password($new_account['password']),
			'old_passwd' => $this->_encrypt_password($new_account['password']),
			'email'=>$new_account['email'],
			'nama_lengkap'=>$new_account['nama_lengkap'],
			'reg_date'=>date('Y-m-d H:i:s'),
			'is_active' => 0,
			'nomor_hp' => $new_account['nomor_hp'],
			'username' => $new_account['username'],
			'is_email_confirmed'=>0,
			'files'=> json_encode($files)
		];

		$this->db->insert('account_register',$account_register);
		$account_id = $this->db->insert_id();

		// create user_map
        $user_map = [
            'user_id' => $account_id,
            't' => '1'
        ];

        $this->db->insert('user_map',$user_map);
        $user_id = $this->db->insert_id();    

        // update uploads
		$this->db->where('owner',$owner)->update('uploads',['user_id'=>$user_id]);

        // send email
        // mail cache
        $current_date = date('Y-m-d H:i:s');
        $stop_date = date('Y-m-d H:i:s', strtotime( "$current_date + 1 day" )); 

        $mail_cache = [
            'user_id' => $user_id,
            'key' => 'mail_confirm',
            'valid_until' => $stop_date,
            'value' => $owner
        ];

        $this->db->insert('caches',$mail_cache);
		$this->_register_send_email($account_register, $owner);


		return [$account_register, $owner];

	}
	function _register_validation_error( ){
		 // echo 'Register:Validation Err';
		 // echo validation_errors();
		$this->__site_layout = 'register_layout';
		$this->view('register_page',['register_failed'=>false,'validation_error'=>validation_errors()]);

	}
	function _register_failed( $msg ){
		$this->__site_layout = 'register_layout';
		$this->view('register_page',['register_failed'=>true,'msg'=>$msg]);

	}
	function _register_success($register){
		$data = [
	 		'nama_lengkap' => $register[0]['nama_lengkap'],
	 		'email' => $register[0]['email'],
	 	];
		redirect('register_success?ref=register&owner='.$register[1].'&data='.base64_encode(json_encode($data)));
	}
	function _register_send_email($account_register, $owner){
		
		$message = $this->load->view('email/email_verifikasi',[],true);
	 	
        $this->_custom_keywords = [
        	'nama_lengkap' =>  $account_register['nama_lengkap'],
        	'link_verifikasi'=> site_url('confirm_mail_v2?owner=').$owner
        ];
        $message = $this->cms_parse_keyword($message);

		$this->_send_email(SMTP_YML['smtp_user'], 'PPSL PERUMDAM TKR', $account_register['email'], 'Verifikasi Email Pendaftaran Baru PPSL PERUMDAM TKR', $message);
	}

	
	public function confirm_mail()
	{
		$get_owner = $this->input->get('owner');
		$owner = $this->session->userdata('upload_owner_register');

		if($owner == $get_owner){
			// echo 'Valid Activation Code : Activation Succes ';
		}
		$upload = $this->db->where('owner',$get_owner)->get('uploads')->row();
		// print_r($upload);
		if(is_object($upload)){
			$reg_id = $upload->user_id;
			// echo $reg_id;
			$this->db->where('id',$reg_id)->update('account_register',['is_email_confirmed'=>1]);

			// redirect('login?ref=aktifasi');

			$this->__site_layout = 'confirm_mail_success_layout';
			$this->view('register_confirm_mail',['register_failed'=>false,'validation_error'=>'','owner'=>$owner]);
	
		}else{
			die('Maaf link sudah Kadaluarsa' );
		}
	}
	public function confirm_mail_v2()
	{
		$owner = $this->input->get('owner');
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','mail_confirm')
							   ->get('caches')->row();
		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';

		$success = false;

		if(!empty($mail_cache)){
			$valid_interval =  strtotime($mail_cache->valid_until) - time();
			if($valid_interval > 0){
				$am = $this->db->where('user_id',$mail_cache->user_id)
							   ->get('account_map')
							   ->row();
				if(!empty($am)){
					$this->db->where('id',$am->parent_id)
						 	 ->update('account_register',['is_email_confirmed'=>1]);
					$success = true;		 	 
					$msg = 'Terima kasih sudah melakukan Konfirmasi Email';
					
				}else{
					$msg = 'Peta user tidak sah';
				}				   
				
			}else{
				$msg = 'Maaf Link sudah kadaluarsa';
			}

			if($valid_interval <= 0){
				$this->db->where('value',$owner)
							 ->where('key','mail_confirm')
							 ->delete('caches');
			}
			$title = $msg;
		}
		$this->__site_layout = 'info_layout';	
		$this->view('register_confirm_mail',['success'=>$success,'msg'=>$msg,'owner'=>$owner,'title'=>$title]);					 
	}
	
    public function register_success()
	{
		$owner = $this->input->get('owner');
		$data = $this->input->get('data');
		$this->__site_layout = 'register_success_layout';
		$this->view('register_sukses',['register_failed'=>false,'validation_error'=>'','owner'=>$owner,'data'=>$data]);
	}
}