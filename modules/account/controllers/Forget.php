<?php
 
class Forget extends Theme_Controller
{
	
	
	public function index($value='')
	{
		 
		if($this->input->get('change_password') == 'true'  ){
    		$id = $this->input->post('user_id');
    		$t = $this->input->post('t');
    		$parent_id = $this->input->post('parent_id');
    		$password = $this->_encrypt_password($this->input->post('password'));
    		$reg_account = $this->session->userdata('reg_account');
  
    		if(is_array($reg_account)){
    			if($id == $reg_account['user_id']){
    				$data = [
						'passwd' => $password
					];
					if($t == 0){
						$table_name = 'account_adm';
					}else if($t == 1){
						$table_name = 'account_register';

					}else if($t == 2){
						$table_name = 'account';

					}

					$this->db->where('id',$parent_id)->update($table_name,$data);
					$this->session->unset_userdata('reg_account');
					die('<div class="alert alert-success"><i class="fa fa-check"></i> Kata sandi berhasil diubah</div>');
			 
    			}
    		}
    		die('Link Expired !');
    	}
		if($this->input->get('form') == 'true'  ){
    		$id = $this->input->get('id');
    		$reg_account = $this->session->userdata('reg_account');
    		$data = $this->input->get('data');
    		$data = json_decode(base64_decode($data));
    		// echo $data;
    		if(is_array($reg_account)){
    			if($data->user_id == $reg_account['user_id']){
    				$data = [
						'page_title' => 'Reset Kata Sandi',
						'user_id' => $data->user_id,
						'email'=>$data->email,
						't' => $data->t,
						'parent_id' => $data->parent_id
					];
					$this->__site_layout = 'single_layout';
					return $this->view('content/forget_form',$data);
    			}
    		}
    		die('Link Expired !');
    	}
    	$email_sent = false;
    	$invalid_email = false;
		if($this->input->get('resend_email') == 'true'){
    		
    		$captcha_error = false;
    		$errMsg = '';
    		$this->form_validation->set_rules('email','Email','required|valid_email');
    		$this->form_validation->set_rules('g-recaptcha-response','Verifikasi','required|valid_email');
    		if($this->form_validation->run()){
    			if(APP_YML['app_enable_google_captcha']){

					if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
					{
					    $secret = APP_YML['app_google_captcha_secret'];
					    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					    $responseData = json_decode($verifyResponse);
					    if(!$responseData->success)
					    {
					        $errMsg = 'Robot verification gagal, silahkan coba lagi.';
					        $captcha_error = true; 
					    }
					}else{
						$errMsg = 'Robot verification gagal, silahkan coba lagi.';
					     $captcha_error = true; 
					}
				}
				if(!$captcha_error){
					$this->_forget_send_email($invalid_email);
    				$email_sent = true;
				}
    		}else{
				$errMsg = validation_errors();
			}
    	}
		$data = [
			'page_title' => 'Lupa Kata Sandi',
			'email_sent' => $email_sent,
			'invalid_email' => $invalid_email,
			'captcha_error' => $captcha_error,
			'message' => $errMsg
		];
		// if(!empty($errMsg)){
		// 	$data['error'] =  '<p>'.$errMsg.'</p>';
		// }
		$this->__site_layout = 'login_layout';
		$this->view('forget_page',$data);
	}
	public function v2_post()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('owner','Owner','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('user_id','User ID','required');
		$this->form_validation->set_rules('parent_id','Account ID','required');
		$this->form_validation->set_rules('passwd','Kata Sandi','required|min_length[6]');
		$this->form_validation->set_rules('repeat_passwd','Ulangi Kata Sandi','required|min_length[6]|matches[passwd]');

		$owner = $this->input->post('owner');
		$email = $this->input->post('email');
		$user_id = $this->input->post('user_id');
		$account_id = $this->input->post('parent_id');
		$passwd = $this->input->post('passwd');

		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';
		$success = false;
		$log = false;
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->where('user_id',$user_id)
							   ->get('caches')->row();
		if($this->form_validation->run()){
			
			if(!empty($mail_cache)){
				$valid_interval =  strtotime($mail_cache->valid_until) - time();
				if($valid_interval>0){ 
				$uppercase = preg_match('@[A-Z]@', $passwd);
	            $lowercase = preg_match('@[a-z]@', $passwd);
	            $number    = preg_match('@[0-9]@', $passwd);

	            if(!($uppercase && $lowercase && $number)) {
	              $msg = 'Kata Sandi Baru tidak valid' . "<br/>";
	            }else{
	            	$am = $this->db->where('user_id',$user_id)->get('account_map')->row();
	            	$group_id = $am->t;
	            	
	            	$table_maps = [
						'0' => 'account_adm',
						'1' => 'account_register',
						'2' => 'account'
					];


					$this->load->model('m_account_v2');
					$this->m_account_v2->change_password($user_id,$am->passwd, $passwd,$am);

					
					$success = true;
	            }
	        	}
			}else{
				// $msg = ''
			}						   
		}else{
			$msg = validation_errors();
			
		}
		

		$this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->where('user_id',$user_id)
							   ->delete('caches');
		$response =[
			'mail_cache' => $mail_cache,
			'msg' => $msg,
			'success' => $success,
			'log' => $log
		];
		echo json_encode($response);
	}
	public function v2()
	{
		$owner = $this->input->get('owner');
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->get('caches')->row();
		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';

		$success = false;

		if(!empty($mail_cache)){
			$valid_interval =  strtotime($mail_cache->valid_until) - time();
			if($valid_interval > 0){
				$am = $this->db->where('user_id',$mail_cache->user_id)
							   ->get('account_map')
							   ->row();
				if(!empty($am)){
					 		 	 
					
					$new_owner = md5(microtime());
					$this->db->where('value',$owner)
							 ->where('key','forget_password')
							 ->update('caches',['value'=>$new_owner]);
					$data = [
						'page_title' => 'Ubah Kata Sandi',
						'user_id' => $am->user_id,
						'email'=>$am->email,
						't' => $am->t,
						'parent_id' => $am->parent_id,
						'owner' => $new_owner
					];
					$this->__site_layout = 'single_layout';
					// $this->_page_title = 'Ubah Kata Sandi'; 
					return $this->view('forget_form_v2',$data);
				}else{
					$msg = 'Peta user tidak sah';
				}				   
				
			}else{
				$msg = 'Maaf Link sudah kadaluarsa';
			}
			$title = $msg;
		}
		$this->__site_layout = 'info_layout';	
		$this->view('forget_info',['success'=>$success,'msg'=>$msg,'owner'=>$owner,'title'=>$title]);	

		
	}
	function _forget_send_email(&$invalid_email){
		

		$message = $this->load->view('email_forget',[],true);
		$email = $this->input->post('email');
		$response = [
			'success' => false,
			'message' => 'Tidak Ada Akun Terkait dengan alamat Email ini',
			'email'=>$email
		];

		$reg_account = $this->db->where('email',$email)->get('account_map')->row_array();
		
		// $owner = 'owner';
		$this->session->set_userdata('reg_account',$reg_account);

		if(is_array($reg_account)){
			$data = ['user_id'=>$reg_account['user_id'],'email'=>$email];
			$link_reset_password = site_url() . 'forget?form=true&user_group=non&data='.base64_encode(json_encode($data));
			$this->_custom_keywords = [
	        	'nama_lengkap' =>  $reg_account['nama_lengkap'],
	        	'link_reset_password'=>  $link_reset_password,
	        	'alamat_email' => $email,
	        	'link_bantuan' => ''
	        	
	        ];
	        $message = $this->cms_parse_keyword($message);

			$this->_send_email(SMTP_YML['smtp_user'], 'PPSL PERUMDAM TKR', $email, 'Permintaan Reset Password PPSL PERUMDAM TKR', $message);
			$response['success'] = true;
			$response['message'] = 'Email berhasil dikirim';
		}

		return $response; 
       
	}
}