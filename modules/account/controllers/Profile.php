<?php

class Profile extends Theme_Controller
{
	public function index()
	{
		$this->load->model('pengaturan/m_pengguna');
		$account = $this->m_pengguna->get_by_id($this->cms_user_id());
		$foto = site_url().'uploads/avatar/'.$account['image'];
 

    	$data = [
    		'account' => $account,
    		'foto' => empty($foto)?site_url().'themes/metronic/assets/pages/media/profile/profile_user.png' :$foto
    	];
    	$this->view('profile',$data);
	}	

	public function change_avatar()
	{
 
		$owner = $this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		$config['upload_path'] = BASE.'/uploads/avatar/';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
    
		$this->load->library('upload', $config);
    
    	$field_name = "file";
        
		if ( ! $this->upload->do_upload($field_name))
		{
			$msg =  $this->upload->display_errors() ;
			echo json_encode(['success'=>false,'message'=>$msg]);
			return false;
		}

		$uploaded_file =(object) $this->upload->data();
		$uploaded_file->success = true;

		$row = $this->db->where('owner',$owner)
							->where('rules','photo_profile')
							->get('uploads')->row();

		if(is_object($row)){
			@unlink(BASE .'/'. $row->file_path);
			$this->db->where('owner',$owner)
					 ->where('rules','photo_profile')
					 ->delete('uploads');
		}
		$upload = [
			'rules' => 'photo_profile',
			'desc'=>'photo_profile',
			'owner' => $owner,
			'user_id' => $owner,
			'file_name' => $uploaded_file->file_name,
			'file_type' => $uploaded_file->file_type,
			'file_ext' => $uploaded_file->file_ext,
			'is_image' => 1,
			'file_size' => $uploaded_file->file_size,
			'image_width'=>$uploaded_file->image_width,
			'image_height'=>$uploaded_file->image_height,
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/avatar/'.$uploaded_file->file_name
		];
		$this->db->insert('uploads',$upload);

		$upload['id'] = $this->db->insert_id();

		echo json_encode($uploaded_file);
	}
	public function updateProfile()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_id','User ID','required|numeric');
		$this->form_validation->set_rules('email','Email','valid_email');
		$this->form_validation->set_rules('nomor_hp','Nomor HP','numeric');
		// $this->form_validation->set_rules('nama_lengkap','Nama Lengkap','numeric');

		$response = [
			'success' => false,
			'message' => ''
		];

		$user_id = $this->input->post('user_id');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$nomor_hp = $this->input->post('nomor_hp');
		
		if($this->form_validation->run()){
			$unique_email = true;
			$unique_nomor_hp = true;

			if(isset($email)){
				$unique_email = $this->_unique_email_for_edit($email,$user_id);
			}
			if(isset($nomor_hp)){
				$unique_nomor_hp = $this->_unique_nomor_hp_for_edit($nomor_hp,$user_id);
			}

			if($unique_email && $unique_nomor_hp){
				$am = $this->db->where('id',$user_id)->get('user_map')->row();
				if(is_object($am)){
					$account_id = $am->user_id;
					$group_id = $am->t;

					switch ($group_id) {
						case '1': // non
				
						$data = [
							
						];
						if(!empty($email)){
							$data['email'] = $email;
						}
						if(!empty($nama)){
							$data['nama_lengkap'] = $nama;
						}
						if(!empty($nomor_hp)){
							$data['nomor_hp'] = $nomor_hp;
						}

						$this->db->where('id',$account_id)->update('account_register',$data);
						break; 
					case '2': // PEGAWAI
						// $data = [];
						
						
						// if(!empty($email)){
						// 	$data['email'] = $email;
						// 	$this->db->where('id',$account_id)->update('account',$data);
						// }

						 
						// $account = $this->db->where('id',$account_id)->get('account')->row();
						// $this->m_api_simpeg->update_profile($account->id_pegawai,$nomor_hp);
						
						break;
					case '0': // adm
						$data = [];
						
						
						if(!empty($email)){
							$data['email'] = $email;
						}
						if(!empty($nama)){
							$data['nama_lengkap'] = $nama;
						}
						

						$this->db->where('id',$account_id)->update('account_adm',$data);

						if(!empty($nomor_hp)){

							$user_info = $this->db->where('user_id',$user_id)
												  ->where('key','nomor_hp')
												  ->get('user_info')
												  ->row();
							if(!empty($user_info)){
								$this->db->where('user_id',$user_id)
										 ->where('key','nomor_hp')
										 ->update('user_info',['value'=>$nomor_hp]);
							}else{
								$this->db->insert('user_info',[
									'key' => 'nomor_hp',
									'value' => $nomor_hp,
									'user_id' => $user_id
								]);	
							}
							 
						}
						break;  
						
					}
					$response['success'] = true;
				}
			}
			if(!$unique_email){
				$response['message'] .= 'Email sudah digunakan' ."<br/>";
			}
			if(!$unique_nomor_hp){
				$response['message'] .= 'Nomor HP sudah digunakan' . "<br/>";
			}

		}else{
			$response['message'] = validation_errors();
		}

		echo json_encode($response);

	}
	 
}