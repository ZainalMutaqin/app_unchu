<?php


class Logout extends Theme_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->session->unset_userdata('account');
		redirect('login');
	}
}