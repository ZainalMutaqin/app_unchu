<?php
class Mailer extends Theme_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	function send_email($from_address, $from_name, $to_address, $subject, $message)
    {
        $this->load->library('email');
        $config['protocol']       = 'smtp';
        $config['smtp_host']      = SMTP_YML['smtp_host'];
        $config['smtp_user']      = SMTP_YML['smtp_user'];
        $config['smtp_pass']      = SMTP_YML['smtp_pass'];
        $config['smtp_port']      = SMTP_YML['smtp_port'];
        $config['mailtype']       = 'html';
        $config['charset']        = 'iso-8859-1';
        $config['validate']       = FALSE;
        $config['crlf']           = "\r\n";
        $config['newline']        = "\r\n";
        $config['smtp_crypto']        = "ssl";
        $this->email->initialize($config);
        $this->email->from($from_address, $from_name);
        $this->email->to($to_address);
        $this->email->subject($subject);
        $this->email->message($message);

        $success = $this->email->send();
       
        if(!$success){
        	log_message('error', $this->email->print_debugger());
        }
        return $success;
    }
}