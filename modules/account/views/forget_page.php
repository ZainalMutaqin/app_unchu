  <!-- BEGIN LOGO -->

 <div class="container">
        
        <div class="logo" >
            <a href="javascript:;">
                <img src="{{ site_url }}pub/img/logo-white.jpg" style="height: auto;" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top: 0">
            <?if(!$email_sent):?>
            <form class="login-form" action="{{ site_url }}lupa-password?resend_email=true" id="login-form" method="post">
                <div class="form-title" style="text-align: center;">
                    <span class="form-title">Masukkan Email Anda</span>
                    <span class="form-subtitle">Kami akan mengirim Link Reset Password ke alamat email Anda. Mohon isi alamat email dengan benar.</span>
                </div>
                 <?if($invalid_email || !empty($message)):?>
                <div class="alert alert-danger login_error">
                    <a href="javascript:;" class="close" data-close="alert"></a>
                    <span> <?=!empty($message)?$message:'Alamat Email Tidak terdaftar.' ?></span>
                </div>
                <?endif?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                 
                <?if(APP_YML['app_enable_google_captcha']):?>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Verifikasi</label>
                    <div class="g-recaptcha " data-sitekey="<?=APP_YML['app_google_captcha_sitekey']?>"></div>
                </div>
                <?endif?>
                <div class="form-actions">
                    <button type="submit" class="btn blue btn-block uppercase">Kirim</button>
                </div>
                 
                     
            </form>
            <?else:?>
            <div>
                <div class="form-title" style="text-align: center;">
                    <span class="form-title">Silahkan Periksa Email Anda</span>
                    <span class="form-subtitle">Tautan Link Reset Password telah dikirim ke alamat email Anda.</span>
                </div>
            </div>
            <?endif?>
            <!-- END LOGIN FORM -->
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(()=>{
        setTimeout(()=>{
             $('#login-form').unbind('submit');
    $('#login-form').submit(()=>{
        console.log('login')
        return true;
    })
            // $('.videobg').get(0).play()
        },1000);
    
        // $(window).resize(()=>{
        //     var h = $(window).height();
        //     $('.container').height(h+30)
        // }).resize();

        $('a[data-close=alert]').click(()=>{
            $('.login_error').hide()
        });
    });
   
</script>

<style type="text/css">
    body{
        overflow: hidden;
    }
    span.form-subtitle{
        display: block;
        text-align: left;
        color: #333 !important;
        font-size: 16px !important;
    }
    span.form-title{
        font-size: 1em !important;
        color: #333 !important;
        display: block; 
    }
    .videobg{
        position: fixed;
        z-index: -1;
    }
    .login{
        background:#fff;
    }
</style>