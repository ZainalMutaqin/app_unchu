<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_konsumen  extends Grocery_CRUD_Model  {
	var $table = 'tb_data_konsumen'; 
    private function _get_datatables_query()
    {
        $this->db->select('a.*,
                            h.nama_propinsi,
                            i.nama_kota,
                            j.nama_kecamatan,
                            k.nama_kelurahan,
                            b.nama_lengkap as nama_pasangan,
                            b.tempat_lahir as tempat_lahir_pasangan,
                            b.tanggal_lahir as tanggal_lahir_pasangan,
                            b.alamat_rumah as alamat_rumah_pasangan,
                            b.no_hp as no_hp_pasangan,
                            b.email as email_pasangan,
                            c.nama_lengkap as nama_keluarga_terdekat,
                            c.alamat as alamat_keluarga_terdekat,
                            c.no_hp as no_hp_keluarga_terdekat,
                            c.hubungan');
        $this->db->from('tb_data_konsumen a');
        $this->db->join('tb_data_pasangan_konsumen b', 'a.id_data_konsumen=b.id_data_konsumen','left');
        $this->db->join('tb_data_keluarga_terdekat c', 'a.id_data_konsumen=c.id_data_konsumen','left');
        $this->db->join('tb_data_pekerjaan_konsumen d', 'a.id_data_konsumen=d.id_data_konsumen','left');
        $this->db->join('tb_data_pekerjaan_pasangan e', 'a.id_data_konsumen=e.id_data_konsumen','left');
        $this->db->join('tb_data_aset f', 'a.id_data_konsumen=f.id_data_konsumen','left');
        $this->db->join('tb_data_pinjaman g', 'a.id_data_konsumen=g.id_data_konsumen','left');
        $this->db->join('tb_propinsi h', 'a.propinsi=h.id_propinsi','left');
        $this->db->join('tb_kota i', 'a.kota=i.id_kota','left');
        $this->db->join('tb_kecamatan j', 'a.kecamatan=j.id_kecamatan','left');
        $this->db->join('tb_kelurahan k', 'a.kelurahan=k.id_kelurahan','left');
        
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
