<!DOCTYPE html>
 
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?=isset($page_title)?$page_title:''?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link rel="stylesheet" type="text/css" href="{{ site_url }}www_static/assets/top_single_layout/css">
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ base_url }}pub/img/favicon.ico">
         
        <script src="{{ theme_assets }}/global/plugins/jquery.min.js" type="text/javascript"></script>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-boxed">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <div class="container">
                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    <!-- BEGIN SIDEBAR -->
                 
                    <!-- END SIDEBAR -->
                    <!-- BEGIN CONTENT -->
                    <?=$template['body']?>
                    <!-- END CONTENT -->
                    <!-- BEGIN QUICK SIDEBAR -->
                    
                    
                    <!-- END QUICK SIDEBAR -->
                <!-- BEGIN FOOTER -->
<div class="page-footer" style="text-align: center;background: #fff;">
    <div class="" style="line-height: 0.7"><?=APP_YML['app_name']?> &copy; <?=date('Y')?> PT Sintech Berkah Abadi - All Rights Reserved.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
                <!-- END FOOTER -->
                </div>
                <!-- END CONTAINER -->

            </div>
            
        </div>
        
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="{{ theme_assets }}/global/plugins/respond.min.js"></script>
<script src="{{ theme_assets }}/global/plugins/excanvas.min.js"></script> 
<script src="{{ theme_assets }}/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
<script type="text/javascript" src="{{ base_url }}www_static/js/app_js"></script>
<script type="text/javascript" src="{{ base_url }}www_static/assets/bottom_single_layout/js"></script>

        
        
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                 
            })
        </script>
    </body>

</html>