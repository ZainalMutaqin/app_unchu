 
<div class="page-header navbar navbar-fixed-top" id="page_header_navbar">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="{{ homepage }}">
                            <img src="{{ theme_assets }}/img/logo-big.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                           <!--  <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 1 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold">1 pending</span> notifications</h3>
                                        <a href="javascript:void()">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                                </a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                          
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="min-height: 50px">
                                    <img v-bind:src="user_avatar" class="img-circle">
                                    
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <div class="user-widget" style="padding: 1em;text-align: center;">
                                            <div class="avatar">
                                    <img v-bind:src="user_avatar" class="img-circle" style="width: 80px">
                                                
                                            </div>
                                        <span class="username username-hide-on-mobile" v-text="user_real_name"> </span>
                                            
                                        </div>
                                        </li>
                                    <li>
                                        <a href="{{ base_url }}account/kelola/">
                                            <i class="icon-user"></i> Kelola Akun Anda </a>
                                    </li>
                                     
                                    <li class="divider"> </li>
                                    
                                    <li>
                                        <a href="{{ base_url }}account/logout">
                                            <i class="icon-key"></i> Keluar</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!-- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
 
            </div>
<script type="text/javascript">
    $(document).ready(function(){
        window.$vmph = new Vue({
            el:"#page_header_navbar",
            data:{
                user_avatar:'{{ user_avatar }}',
                user_real_name:'{{ user_real_name }}'
            },
            ready:function(){
                var updated_real_name = '<?=$this->input->cookie('cms_user_real_name',TRUE)?>';
                if(updated_real_name.length > 0){
                    this.user_real_name = updated_real_name;
                }
                var cms_user_avatar = Cookies.get('cms_user_avatar');
                if(typeof cms_user_avatar != 'undefined'){
                    this.user_avatar = cms_user_avatar;
                }
            }
        });
    });
</script>