<?if(cms_is_admin() && uri_match(['manajemen_user','data_marketing'])):?>
<nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                <li>
                    <a href="javascript:;" id="syc_account_btn" class="active">
                        <span>Sync Account</span>
                        <i class="icon-users"></i>
                    </a>
                </li>
               
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
<div class="quick-nav-overlay"></div>
<!-- Modal -->
<div class="modal fade" id="syncAccountModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"  v-if="!is_processing"></button>
                <h4 class="modal-title"><i class="fa fa-users"></i> Sync Account</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" v-if="process_complete">
                    <p><i class="fa fa-check"></i> Sync Selesai</p>
                </div>
                <div  v-if="!process_complete">
                    <div class="row" v-if="!is_processing">
                        <div class="form-group col-md-4">
                            <label>Total Records</label>
                            <input type="text" v-model="total_records" class="form-control input-small" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Per Page</label>
                            <select id="data_per_page" v-model="per_page" class="form-control input-small" @change="calculateTotalPages()">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Total Pages</label>
                            <input type="text" v-model="total_pages" class="form-control input-xs" disabled>
                        </div>
                    </div>
                    <div class="row" v-if="is_processing">
                        <div class="form-group col-md-3">
                            <label>Per Page</label>
                            <input type="text" v-model="per_page" class="form-control input-xs" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Current Page</label>
                            <input type="text" v-model="page" class="form-control input-xs" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Total Records</label>
                            <input type="text" v-model="pp_records" class="form-control input-xs" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Total Pages</label>
                            <input type="text" v-model="pp_pages" class="form-control input-xs" disabled>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer row" style="margin: 0" v-if="!process_complete">
                <div class="col-md-8">
                    <div class="progress progress-striped active" v-if="is_processing">
                        <div class="progress-bar progress-bar-success" role="progressbar" :aria-valuenow="p_now" :aria-valuemin="p_min" :aria-valuemax="p_max" :style="'width: '+p_now+'%'">
                            <span class="sr-only"> {{p_now}}% Complete (success) </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                <button type="button" class="btn btn-default" :disabled="is_processing" @click="main_proc()"><i :class="{'fa fa-cog':!is_processing,'fa fa-spinner fa-spin':is_processing}"></i> Proses</button>
                    
                </div>
            </div>
            <div class="modal-footer row" style="margin: 0" v-if="process_complete">
                <button type="button" class="btn btn-default"  data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>

            </div>    
            <div class="script"></div>
        </div>
    </div>
</div>
<?endif?>