<?php
 
class Mypdo
{
	protected $_ci;
	protected $_db;
	protected $_query;
	public function __construct()
	{
		$this->_ci =& get_instance();
		$this->_init_db();
	}
	protected function _init_db(){
		// echo 'hello this is pdo';
		$host = $this->_ci->db->hostname; 
		$User = $this->_ci->db->username;       
		$Password = $this->_ci->db->password;      
		$Database = $this->_ci->db->database;     	

		try{
			$this->_db = new PDO("mysql:dbname=$Database;host=$host;charset=UTF8",$User,$Password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));    		
		    
		}catch(PDOException $ex){

			die(json_encode(array('outcome' => false, 'message' => 'Database connection failed')));   
		}

		$this->_ci->pdo = $this->_db;
	}
	public function prepare($sql)
	{
		return $this->_db->prepare($sql);
	}
	public function get_instance()
	{
		return $this->_db;
	}
}