/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_unchu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-12-10 05:58:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `uploads`
-- ----------------------------
DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rules` varchar(50) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `owner` varchar(200) NOT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `file_size` double DEFAULT NULL,
  `file_ext` varchar(20) DEFAULT '',
  `file_path` varchar(500) DEFAULT '',
  `file_type` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_image` int(11) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `is_tmp` int(11) DEFAULT NULL,
  `parent_id` varchar(225) DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of uploads
-- ----------------------------
